<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Product_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function get_data(){
        $this->db->select('a.*, b.sub_category, c.category');
        $this->db->from('product a');
        $this->db->join('sub_category b', 'a.sub_categoryID=b.id');
        $this->db->join('category c', 'b.categoryID=c.id');
        $this->db->where('a.flag', 0);
        $this->db->where('b.flag', 0);
        $this->db->where('c.flag', 0);
        $this->db->order_by('a.id', 'desc');
        $this->db->group_by('a.product');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_data_for_index(){
        $this->db->select('a.*, b.sub_category, c.category');
        $this->db->from('product a');
        $this->db->join('sub_category b', 'a.sub_categoryID=b.id');
        $this->db->join('category c', 'b.categoryID=c.id');
        $this->db->where('a.flag', 0);
        $this->db->where('b.flag', 0);
        $this->db->where('c.flag', 0);
        $this->db->order_by('a.id', 'desc');
        $this->db->group_by('a.product');
        $this->db->limit('12');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_data_by_sub_category($id){
        $this->db->select('a.*, b.sub_category, c.category');
        $this->db->from('product a');
        $this->db->join('sub_category b', 'a.sub_categoryID=b.id');
        $this->db->join('category c', 'b.categoryID=c.id');
        $this->db->where('a.sub_categoryID', $id);
        $this->db->where('a.flag', 0);
        $this->db->where('b.flag', 0);
        $this->db->where('c.flag', 0);
        $this->db->group_by('a.product');
        $this->db->order_by('a.id', 'desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_data_by_seller($id){
        $this->db->select('a.*, b.sub_category, c.category');
        $this->db->from('product a');
        $this->db->join('sub_category b', 'a.sub_categoryID=b.id');
        $this->db->join('category c', 'b.categoryID=c.id');
        $this->db->where('a.sellerID', $id);
        $this->db->where('a.flag', 0);
        $this->db->where('b.flag', 0);
        $this->db->where('c.flag', 0);
        $this->db->order_by('a.id', 'desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_data_for_copy(){
        $this->db->select('a.*, b.sub_category, c.category');
        $this->db->from('product a');
        $this->db->join('sub_category b', 'a.sub_categoryID=b.id');
        $this->db->join('category c', 'b.categoryID=c.id');
        $this->db->where('a.isCopy', 0);
        $this->db->where('a.flag', 0);
        $this->db->where('b.flag', 0);
        $this->db->where('c.flag', 0);
        $this->db->order_by('a.id', 'desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_details($id){
        $this->db->select('a.*, b.name, b.mobile, b.email, c.category, d.sub_category, bb.name as copyName');
        $this->db->from('product a');
        $this->db->join('seller b','a.sellerID=b.id');
        $this->db->join('seller bb','a.isCopy=bb.id', 'left');
        $this->db->join('category c','a.categoryID=c.id');
        $this->db->join('sub_category d','a.sub_categoryID=d.id');
        $this->db->where('a.id', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function get_details_by_product($product){
        $this->db->select('a.*, b.name, b.mobile, b.email, c.category, d.sub_category');
        $this->db->from('product a');
        $this->db->join('seller b','a.sellerID=b.id');
        $this->db->join('category c','a.categoryID=c.id');
        $this->db->join('sub_category d','a.sub_categoryID=d.id');
        $this->db->where('a.product', $product);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_sub_category(){
        $this->db->select('*');
        $this->db->from('sub_category');
        $this->db->where('flag', 0);
        $this->db->order_by('sub_category', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function add($data){
        $this->db->insert('product', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function edit($data, $id){
        $this->db->where('id', $id);
        $this->db->update('product', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('product');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}