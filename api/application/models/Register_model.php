<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Register_model extends CI_Model
{     
    function check($check, $type){
        $this->db->select('a.*');
        $this->db->from('buyer a');
        if ($type == 'email') {
            $this->db->where('a.email', $check);
        } else if ($type == 'username') {
            $this->db->where('a.username', $check);
        }
        
        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()) {
            $row = $query->row_array();
            if ($type == 'email') {
                return [
                    'id'       => $row['id'],
                    'email'    => $row['email'],
                ];
            } else if ($type == 'username') {
                return [
                    'id'       => $row['id'],
                    'username' => $row['username'],
                ];
            }
        }
        
        return [];
    }

    function signup($data){
        $this->db->insert('buyer', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
}