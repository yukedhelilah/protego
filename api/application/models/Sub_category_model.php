<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Sub_category_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function get_data(){
        $this->db->select('a.*, b.category');
        $this->db->from('sub_category a');
        $this->db->join('category b','a.categoryID=b.id');
        $this->db->where('a.flag',0);
        $this->db->where('b.flag',0);
        $this->db->order_by('a.id', 'desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_data_by_category($id){
        $this->db->select('a.*, b.category');
        $this->db->from('sub_category a');
        $this->db->join('category b','a.categoryID=b.id','left-join');
        $this->db->where('a.categoryID', $id);
        $this->db->where('a.flag',0);
        $this->db->where('b.flag',0);
        $this->db->order_by('a.id', 'desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_category(){
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('flag', 0);
        $this->db->order_by('category', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_details($id){
        $this->db->select('*');
        $this->db->from('sub_category');
        $this->db->where('id', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function add($data){
        $this->db->insert('sub_category', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function edit($data, $id){
        $this->db->where('id', $id);
        $this->db->update('sub_category', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('sub_category');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}