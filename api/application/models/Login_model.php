<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Login_model extends CI_Model
{   

    function login($username, $password){
        $this->db->select('*');
        $this->db->from('buyer');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->where('flag', '0');

        $query = $this->db->get();

        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows() > 0) {
            $row = $query->row_array();

            $tokenKey = $row['username'] . '|' . date('Y-m-d H:i:s');
            return [
                'id'        => $row['id'],
                'name'      => $row['name'],
                'username'  => $row['username'],
                'token'     => md5($tokenKey),
                'expired'   => date("Y-m-d"),
            ];
        } else {
            return NULL;
        }
        
        return []; 
    }  
    
    function forgot_password($username){
        $this->db->select('*');
        $this->db->from('buyer');
        $this->db->where('flag',0);
        $this->db->where('username',$username);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function input_auth($auth,$id){
        $object = array(
            'authenticationCode'    => $auth,
        );
        
        $this->db->where('id', $id);
        $this->db->where('flag', 0);
        $this->db->update('buyer', $object);

        if($this->db->affected_rows()){
            $this->db->select('authenticationCode');
            $this->db->from('buyer');
            $this->db->where('id',$id);

            $query = $this->db->get();
            
            if (!$query) {
                return ['error' => $this->db->error()];
            }
            
            if ($query->num_rows()>0) {
                $row = $query->row_array();
                return $row;
            }
        }else{
            return false;
        }
    }

    function reset_password($id,$data){
        $this->db->where('id',$id);
        $this->db->update('buyer', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            // any trans error?
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}