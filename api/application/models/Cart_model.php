<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Cart_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function get_data($id){
        $this->db->select('a.*, b.product, b.price, b.img1, b.sellerID');
        $this->db->from('cart a');
        $this->db->join('product b','a.productID=b.id');
        $this->db->where('a.buyerID', $id);
        $this->db->where('a.status', 0);
        $this->db->order_by('a.id', 'desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_total($id){
        $this->db->select('count(*) as total');
        $this->db->from('cart');
        $this->db->where('buyerID', $id);
        $this->db->where('status', 0);
        $this->db->order_by('id', 'desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function check_product($productID, $buyerID){
        $this->db->select('*');
        $this->db->from('cart');
        $this->db->where('productID', $productID);
        $this->db->where('buyerID', $buyerID);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function get_details($id){
        $this->db->select('a.*, a.productID, b.sellerID, b.sub_categoryID, c.categoryID');
        $this->db->from('cart a');
        $this->db->join('product b','b.id=a.productID','left');
        $this->db->join('sub_category c','c.id=b.sub_categoryID','left');
        $this->db->where('a.id', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function add($data){
        $this->db->insert('cart', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function edit($data, $id){
        $this->db->where('id', $id);
        $this->db->update('cart', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('cart');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}