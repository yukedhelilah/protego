<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Category_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function get_all(){
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('flag', 0);
        $this->db->order_by('category', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();

            foreach ($row as $key => $value) {
                $this->db->select('id, sub_category');
                $this->db->from('sub_category');
                $this->db->where('categoryID', $value->id);
                $this->db->where('flag', 0);
                $this->db->order_by('sub_category', 'asc');
                // $this->db->group_by('a.claimID');

                $rows           = $this->db->get();
                $sub_category   = $rows->result();

                $arr[$key] = [
                    'id'            => $value->id,
                    'category'      => $value->category,
                    'sub_category'  => $sub_category,
                ];
            }

            return $arr;
        }
    }

    function get_data(){
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('flag', 0);
        $this->db->order_by('id', 'desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_details($id){
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('id', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function add($data){
        $this->db->insert('category', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function edit($data, $id){
        $this->db->where('id', $id);
        $this->db->update('category', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('category');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}