<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends App_Public {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('Product_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data = $this->mainmodul->get_data();

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_data_for_index_post(){
        $data = $this->mainmodul->get_data_for_index();

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_data_by_sub_category_post(){
        $data = $this->mainmodul->get_data_by_sub_category($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_data_by_seller_post(){
        $data = $this->mainmodul->get_data_by_seller($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_data_for_copy_post(){
        $data = $this->mainmodul->get_data_for_copy();

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_sub_category_post(){
        $data = $this->mainmodul->get_sub_category();

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_details_post(){
        $data       = $this->mainmodul->get_details($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_details_by_product_post(){
        $data       = $this->mainmodul->get_details_by_product($this->input->post('product'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function uploadImg_post(){
        $uploadpath = 'files/product/';
        $status = "";
        $msg    = "";
        $file_element_name = 'file_img';

        if ($status != "error")
        {
            $config['upload_path']      = $uploadpath;
            $config['allowed_types']    = 'gif|jpg|jpeg|png';
            $config['max_size']         = 1024 * 8;

            $new_name = 'product_'.time().'_'.$_POST['no'];
            $config['file_name'] = $new_name;

            $this->load->library('upload', $config);
      
            //$this->upload->initialize($config);
      
            if (!$this->upload->do_upload($file_element_name)){
                $status = 'error';
                $msg    = $this->upload->display_errors('', '');
            }else{
                $img    = $this->upload->data();
                $status = '';
                $msg    = $img['file_name'];
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo $status.'*'.$msg.'*'.$_POST['no'];
    }

    public function save_post(){
        $sql        = true;
        $act        = $this->input->post('act', true);

        $data=array(
            'sellerID'          => $this->input->post('sellerID', true),
            'categoryID'        => $this->input->post('categoryID', true),
            'sub_categoryID'    => $this->input->post('sub_categoryID', true),
            'product'           => strtoupper($this->input->post('product', true)),
            'price'             => $this->input->post('price', true),
            'description'       => $this->input->post('description', true),
            'blibli'            => $this->input->post('blibli', true),
            'bukalapak'         => $this->input->post('bukalapak', true),
            'lazada'            => $this->input->post('lazada', true),
            'shopee'            => $this->input->post('shopee', true),
            'tokopedia'         => $this->input->post('tokopedia', true),
            'lainnya'           => $this->input->post('lainnya', true),
            'lainnya2'          => $this->input->post('lainnya2', true),
            'img1'              => $this->input->post('img1', true),
            'img2'              => $this->input->post('img2', true),
            'img3'              => $this->input->post('img3', true),
            'img4'              => $this->input->post('img4', true),
            'img5'              => $this->input->post('img5', true),
            'isCopy'            => $this->input->post('isCopy', true),
            'flag'              => 0,
        );

        if($act == 'add'){
            $sql                    = $this->mainmodul->add($data);
        }else{
            $sql                    = $this->mainmodul->edit($data, $this->input->post('id', true));
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                'total'     => count($sql),
            ], 200);
        }
    }

    public function delete_post(){
        $sql        = true;

        $data       = array(
            'flag'  => 1,
        );

        $sql        = $this->mainmodul->edit($data, $this->input->post('id'));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                'total'     => count($sql),
            ], 200);
        }
    }
}

