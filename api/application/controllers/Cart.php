<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Cart_model', 'mainmodul');
        $this->load->model('Preorder_model', 'secondmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data = $this->mainmodul->get_data($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_total_post(){
        $data = $this->mainmodul->get_total($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_details_post(){
        $data       = $this->mainmodul->get_details($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function save_post(){
        $sql        = true;
        $act        = $this->input->post('act', true);

        $product    = $this->mainmodul->check_product($this->input->post('productID'), $this->input->post('buyerID'));

        if($act == 'edit'){
            if ($this->input->post('type', true) == 'qty') {
                $data=array(
                    'qty'       => $this->input->post('qty', true),
                    'amount'    => $this->input->post('amount', true),
                );
            }
            $sql    = $this->mainmodul->edit($data, $this->input->post('id', true));
        }else{
            if ($product == null) {
                $data=array(
                    'productID'     => $this->input->post('productID', true),
                    'buyerID'       => $this->input->post('buyerID', true),
                    'qty'           => $this->input->post('qty', true),
                    'amount'        => $this->input->post('amount', true),
                    'totalAmount'   => $this->input->post('totalAmount', true),
                );
                $sql    = $this->mainmodul->add($data);
            } else {
                $qty            = $this->input->post('qty', true) + $product['qty'];
                $totalAmount    = $product['amount'] * $qty;
                $data = array(
                    'qty'       => $qty,
                    'amount'    => $product['amount'],
                );
                $sql    = $this->mainmodul->edit($data, $product['id']);
            }
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                'total'     => count($sql),
            ], 200);
        }
    }

    public function addToPreorder_post(){
        foreach ($this->input->post('id') as $key => $value) {
            $array = explode("|", $value);
            $cart = $this->mainmodul->get_details($array[0]);
            $data = array(
                'sellerID'      => $cart['sellerID'],
                'buyerID'       => $cart['buyerID'],
                'categoryID'    => $cart['categoryID'],
                'sub_categoryID'=> $cart['sub_categoryID'],
                'productID'     => $cart['productID'],
                'qty'           => $cart['qty'],
                'amount'        => $cart['amount'],
                'type'          => '',
                'remarks'       => $array[1],
                'deadline'      => date('Y-m-d', strtotime('+2 day')),
                'status'        => 0,
            );
            $sql    = $this->secondmodul->add($data);

            $data2  = array(
                'status'        => 1,
                'remarks'       => $array[1],
            );
            $sql2   = $this->mainmodul->edit($data2, $array[0]);
        }

        $this->response([
            'status'    => (!empty($cart) ? true : false),
            'total'     => count($cart),
            'data'      => $cart,
        ], 200);
    }

    public function delete_post(){
        $sql        = $this->mainmodul->delete($this->input->post('id'));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                'total'     => count($sql),
            ], 200);
        }
    }
}

