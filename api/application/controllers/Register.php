<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends App_Public {

    public function __construct(){
        // header('Access-Control-Allow-Origin: *');
        // header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
        
        $this->load->model('Register_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function check_post(){
        $data = $this->mainmodul->check(strtolower($this->input->post('check')), $this->input->post('type'));
        if (isset($data['error'])) {
            $this->response([
                'code'      => 500,
                'status'    => 'Internal Server Error',
                'info'      => $data['error'],
            ], 200);
        }
        
        $this->response([
            'code'      => 200,
            'status'    => 'Success',
            'data'      => $data,
            'total'     => count($data) > 0 ? count($data) : 0,
        ], 200);
    }

    public function signup_post(){
        $sql  = true;

        $data = array(
            'name'          => strtoupper($this->input->post('name', true)),
            'email'         => strtolower($this->input->post('email', true)),
            'mobile'        => $this->input->post('mobile', true),
            'username'      => strtolower($this->input->post('username', true)),
            'password'      => md5(md5($this->input->post('password', true))),
            'flag'          => 0,
        );
        $sql  = $this->mainmodul->signup($data);

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                'total'     => count($sql),
                'error'     => $this->db->error(),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                'total'     => count($sql),
                'error'     => $this->db->error(),
            ], 200);
        }
    }
}

