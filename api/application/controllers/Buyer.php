<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Buyer extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Buyer_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data = $this->mainmodul->get_data();

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_details_post(){
        $data       = $this->mainmodul->get_details($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function save_post(){
        $sql        = true;
        $act        = $this->input->post('act', true);

        $data=array(
            'name'          => strtoupper($this->input->post('name', true)),
            'username'      => strtolower($this->input->post('username', true)),
            'email'         => $this->input->post('email', true),
            'mobile'        => $this->input->post('mobile', true),
            'address'       => $this->input->post('address', true),
            'province'      => $this->input->post('province', true),
            'city'          => $this->input->post('city', true),
            'subDistrict'   => $this->input->post('subDistrict', true),
            'zipCode'       => $this->input->post('zipCode', true),
            'flag'          => 0,
        );

        if($act == 'add'){
            $data['password']   = md5(md5($this->input->post('password', true)));
            $sql                = $this->mainmodul->add($data);
        }else{
            if ($this->input->post('password') != '') {
                $data['password'] = md5(md5($this->input->post('password', true)));
            }
            $sql                  = $this->mainmodul->edit($data, $this->input->post('id', true));
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                'total'     => count($sql),
            ], 200);
        }
    }

    public function delete_post(){
        $sql        = true;

        $data       = array(
            'flag'  => 1,
        );

        $sql        = $this->mainmodul->edit($data, $this->input->post('id'));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                'total'     => count($sql),
            ], 200);
        }
    }
}

