<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Preorder extends App_Public {
    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();

        $this->load->model('Preorder_model', 'mainmodul');

    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data = $this->mainmodul->get_data();

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_data_bysellerID_post(){
        $data = $this->mainmodul->get_data_bysellerID($this->input->post("id"));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_details_post(){
        $data       = $this->mainmodul->get_details($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function save_post(){
        $sql        = true;
        $act        = $this->input->post('act', true);

        $data=array(
            'sellerID'          => $this->input->post('sellerID', true),
            'categoryID'        =>$this->input->post('categoryID', true),
            'sub_categoryID'    =>$this->input->post('sub_categoryID', true),
            'productID'         => $this->input->post('productID', true),
            'qty'               => $this->input->post('qty', true),
            'type'              => $this->input->post('type', true),
            'remarks'           => $this->input->post('remarks', true),
            'deadline'          => $this->input->post('deadline', true),
            'status'            => $this->input->post('status', true),
        );

        if($act == 'add'){
            $sql                = $this->mainmodul->add($data);
        }else{
            $sql                  = $this->mainmodul->edit($data, $this->input->post('id', true));
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                'total'     => count($sql),
            ], 200);
        }
    }
}
