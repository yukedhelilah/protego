$(document).ready(function() {
});

function requiredField(val,field){
    var value   = $.trim(val);

    if (field == 'email') {
        var re = /\S+@\S+\.\S+/;
        if (re.test(value) == true){
            $.ajax({
                type: 'POST',
                dataType: 'json',
                cache: false,
                url: 'https://protegoindonesia.com/server/Register/check/_',
                data: {
                    check : value,
                    type  : 'email',
                },
                success: function (responses) {
                    if(responses.code == 200 && responses.total == 0){
                        valid(field);
                        $('#txt_alert').css('display','none');
                    }else {
                        invalid(field);
                        $('#txt_alert').css('display','block');
                        $('#txt_alert').html('<b>Alert</b><br>Email already exist');
                    }
                }
            });
        } else { 
            invalid(field);
            $('#txt_alert').css('display','none');
        }
    } else if (field == 'mobile') {
        if (value.length == 1 && value == '0') { $('#mobile').val('62');}
        if (value.length > 9) { valid(field);} else { invalid(field);}
    } else if (field == 'username'){
        if(value.length > 5){
            $.ajax({
                type: 'POST',
                dataType: 'json',
                cache: false,
                url: 'https://protegoindonesia.com/server/Register/check/_',
                data: {
                    check : value,
                    type  : 'username',
                },
                success: function (responses) {
                    if(responses.code == 200 && responses.total == 0){
                        valid(field);
                        $('#txt_alert').css('display','none');
                    }else {
                        invalid(field);
                        $('#txt_alert').css('display','block');
                        $('#txt_alert').html('<b>Alert</b><br>Username already exist');
                    }
                }
            }); 
        } else {
            invalid(field);
            $('#txt_alert').css('display','none');
        }
    } else if (field == 'password') {
        if (value.length > 5) { valid(field);} else { invalid(field);}
    } else {
        if(value.length > 2) { valid(field);} else { invalid(field);}
    }
}

function valid(field){
    $('#'+field).css('border-color','green');
    $('#is'+field).val(1);
}

function invalid(field){
    $('#'+field).css('border-color','red')
    $('#is'+field).val(0);
}

function signup(){
    if ($('#isname').val() == 0 || $('#isemail').val() == 0 || $('#ismobile').val() == 0 || $('#isusername').val() == 0 || $('#ispassword').val() == 0) {
        $('#txt_alert').css('display','block');
        $('#txt_alert').html('<b>Alert</b><br>Your data is not valid');
    } else {
        $('#txt_alert').css('display','none');
        $.ajax({
            type: 'post',
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded", 
            url: 'https://protegoindonesia.com/server/Register/signup/_',
            crossDomain: true, 
            async: false,
            data: $("#form_signup").serialize(),
            beforeSend: function() {
                $('#txt_alert').css('display','none');
                $('#btn_signup').html(`<div class="spinner-border spinner-border-sm text-light" role="status"></div>`);
            },
            success: function(responses) {
                if(responses.code==200){
                    window.location.href = 'login';
                }else{
                    alert('Failed, Try again later');
                    window.location.href = 'register';
                }
            },
        });
    }
}