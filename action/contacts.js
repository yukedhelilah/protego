$(document).ready(function() {
    $('.mobile-menu').css('color','#222');
    $('#contacts-menu').css('color','#9cc15c');
  	get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Seller/get_data/_',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $.each(response.data, function( i, val ) {
      	var img = '';
      	if (val.img1 == null) {
      		img = `https://protegoindonesia.com/server/files/no-image.png`;
      	} else {
      		img = `https://protegoindonesia.com/server/files/product/`+val.img1;
      	}
      	$('#content').append(`
            <div class="col-md-3 col-ts-12" style="margin-top:10px">
              <a href="store?search=`+btoa(val.id+`|`+val.name)+`">
                <div class="furgan-iconbox style-01" style="margin:5px 0">
                    <div class="iconbox-inner">
                        <div class="content">
                            <h4 class="title" style="font-size:14px">`+val.name+`.</h4>
                            <div class="desc" style="font-size:14px">
                            	`+val.email+`<br>
                            	`+val.mobile+`
                            </div>
                        </div>
                    </div>
                </div>
              </a>
            </div>
        `);
      });
    },
  });
}