$(document).ready(function() {
    $('.mobile-menu').css('color','#222');
    $('#account-menu').css('color','#9cc15c');

    get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    url: 'https://protegoindonesia.com/server/Buyer/get_details/_',
    dataType: 'JSON',
    data : {
      id : buyerID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(responses) {
      $('#name').val(responses.data.name);
      $('#username').val(responses.data.username);
      $('#email').val(responses.data.email);
      $('#mobile').val(responses.data.mobile);
      $('#province').val(responses.data.province);
      $('#city').val(responses.data.city);
      $('#subDistrict').val(responses.data.subDistrict);
      $('#zipCode').val(responses.data.zipCode);
      $('#address').val(responses.data.address);
    }
  });
}

function save(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Buyer/save/_',
    data: $('#form_master').serialize()+"&id="+buyerID,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.code == 200) {
        window.location.href = '../pages/account';
      } else {
        alert('failed, try again later.');
      }
    },
  });
}