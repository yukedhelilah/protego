var url     = atob(getUrlParameter('search')).split('|');

$(document).ready(function() {
  $('.mobile-menu').css('color','#222');
  $('#products-menu').css('color','#9cc15c');
	$('#nav-category').html(url[1]);
	$('#title').html(url[1]);
	get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Sub_category/get_data_by_category/_',
    data: {
    	id : url[0],
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $.each(response.data, function( i, val ) {
      	var img = '';
      	if (val.img == null) {
      		img = `https://protegoindonesia.com/server/files/no-image.png`;
      	} else {
      		img = `https://protegoindonesia.com/server/files/product/`+val.img;
      	}
      	$('#content').append(`
            <div class="col-ts-6 col-md-3" style="margin:15px 0;">
                <div class="categories-inner">
                    <figure class="categories-thumb">
                        <a href="products?search=`+btoa(val.id+`|`+url[1]+`|`+val.sub_category+`|`+url[0])+`" tabindex="-1">
                            <img src="`+img+`" class="img-responsive attachment-370x370 size-370x370" alt="img" width="370" height="370"> 
                        </a>
                    </figure>
                    <h3 class="title">
                        <a href="products?search=`+btoa(val.id+`|`+url[1]+`|`+val.sub_category+`|`+url[0])+`" tabindex="-1">`+ val.sub_category +`</a>
                    </h3>
                </div>
            </div>
        `);
      });
    },
  });
}