$(document).ready(function() {
    $('.mobile-menu').css('color','#222');
    $('#cart-menu').css('color','#9cc15c');

    get_data();
    get_profile();

	$('#checkAll').change(function() {
		if ($('#checkAll').is(':checked')) {
			$(".checkCart").prop('checked', true);
		} else {
			$(".checkCart").prop('checked', false);
		}
	});
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Cart/get_data/_',
    data: {
    	id : buyerID,
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
    	if (response.data != null) {
	    	var grandTotal = 0 ;
	      	$.each(response.data, function( i, val ) {
		      	var img = '';
		      	if (val.img1 == '') {
		      		img = `https://protegoindonesia.com/server/files/no-image.png`;
		      	} else {
		      		img = `https://protegoindonesia.com/server/files/product/`+val.img1;
		      	}
		      	$('#content').append(`
		            <div class="col-ts-12 col-md-12" style="margin:15px 0 0 0;border-top: 1px solid #e0e0e0;padding: 15px 10px 0 10px">
			            <div class="row">
			            	<div class="col-ts-5 col-sm-5 col-md-2">
			            		<div class="row">
			                        <div class="col-ts-2 col-sm-2 col-md-4">
				            			<input class="input-checkbox checkCart" type="checkbox" id="`+val.id+`" name="checkCart" style="top: 50%;transform: translateY(-50%);position: relative;">
			                        </div>
			                        <div class="col-ts-10 col-sm-10 col-md-8">
			                            <img class="img-responsive" src="`+ img +`" alt="" width="100%">
			                        </div>
			            		</div>
			            	</div>
			            	<div class="col-ts-7 col-sm-7 col-md-10">
			            		<div class="row" style="top: 50%;transform: translateY(-50%);position: relative;">
			            			<div class="col-ts-10 col-sm-10 col-md-11" style="padding-left:20px">
			            				<div class="row">
					            			<div class="col-ts-12 col-sm-12 col-md-5" style="padding:0;text-align:left">
						                        <h3 class="product-name product_title align-left-mobile" style="text-align: left;">
						                            <a href="#" tabindex="0">`+ val.product +`
						                           	</a>
						                        </h3>
						                        <input type="text" id="remarks`+val.id+`" name="remarks`+val.id+`" style="width:90%;height:30px;padding:2px 5px;margin-top:10px" placeholder="Remarks">
					            			</div>
					            			<div class="col-ts-12 col-sm-12 col-md-2 align-left-mobile mobile-none">
						                        <span class="price" style="top: 50%;transform: translateY(-50%);position: relative;">
						                            <span class="furgan-Price-amount amount">
						                                <span class="furgan-Price-currencySymbol">Rp </span> `+ numberFormat(val.amount) +`
						                            </span>
						                        </span>
					            			</div>
					            			<div class="col-ts-12 col-sm-12 col-md-2 align-left-mobile">
						                        <span class="price" style="top: 50%;transform: translateY(-50%);position: relative;">
						                            <span class="furgan-Price-amount amount">
						                                <span class="furgan-Price-currencySymbol">Rp </span> <span id="totalAmount`+val.id+`">`+ numberFormat(val.totalAmount) +`</span>
						                            </span>
						                        </span>
					            			</div>
					            			<div class="col-ts-12 col-sm-12 col-md-3 align-left-tab" style="padding-left:7%">
					                        	<div onclick="qtyMin(`+val.id+`,`+val.qty+`,`+val.amount+`)" class="btn-solid btn-numeric" style="line-height: 0.82;font-size: 26px;top: 50%;transform: translateY(-50%);position: relative;cursor:pointer">_</div>
					                        	<div id="qty`+val.id+`" class="btn-solid" style="border-right: 1px solid white;border-left: 1px solid white;font-size: 20px;line-height: 1.8;width: 60px;top: 50%;transform: translateY(-50%);position: relative;">`+val.qty+`</div>
					                        	<div onclick="qtyPlus(`+val.id+`,`+val.qty+`,`+val.amount+`)" class="btn-solid btn-numeric" style="top: 50%;transform: translateY(-50%);position: relative;cursor:pointer">+</div>
					            			</div>
			            				</div>
			            			</div>
			            			<div class="col-ts-2 col-sm-2 col-md-1">
			            				<div class="row" style="top: 50%;transform: translateY(-50%);position: relative;">
					            			<div class="col-ts-12 col-sm-12 col-md-12 align-left-mobile" style="padding-left:35%">
					                        	<div onclick="delete_data(`+val.id+`)" class="btn-red" style="text-align:center"><i class="fa fa-trash"></i></div>
					            			</div>
			            				</div>
			            			</div>
			            		</div>
			            	</div>
			            </div>
		            </div>
		        `);

		        grandTotal += parseInt(val.totalAmount);
	      	});

			$('#grandTotal').html(`<span class="furgan-Price-currencySymbol">Rp </span>`+ numberFormat(grandTotal));
    	} else {
    		$('#content').html(`
	            <div class="col-ts-12 col-md-12" style="margin:15px 0 0 0;border-top: 1px solid #e0e0e0;padding: 15px 10px 0 10px">
		        	--- There are no products to checkout ---
		        </div>
            `);
    	}
    },
  });
}

function get_profile(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Buyer/get_details/_',
    data: {
    	id : buyerID,
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
    	$('#address').val(response.data.address);
    	$('#province').val(response.data.province);
    	$('#city').val(response.data.city);
    	$('#subDistrict').val(response.data.subDistrict);
    	$('#zipCode').val(response.data.zipCode);
    },
  });
}

function qtyMin(id, qty, amount){
	if (qty == 1) {
		alert('Minimum order 1pcs');
	} else {
		qty2 		= qty - 1; 
		$.ajax({
			type: 'POST',
			dataType: 'JSON',
			url: 'https://protegoindonesia.com/server/Cart/save/_',
			data: {
				id 			: id,
				qty 		: qty2,
				amount		: amount,
				act 		: 'edit',
				type 		: 'qty',
			},
			headers: {
			    'Content-Type': 'application/x-www-form-urlencoded'
			},
			success: function(response) {
				$('#content').empty();
				get_data();
			},
		});
	}
}

function qtyPlus(id, qty, amount){
	qty2 = qty + 1; 
	$.ajax({
		type: 'POST',
		dataType: 'JSON',
		url: 'https://protegoindonesia.com/server/Cart/save/_',
		data: {
			id 			: id,
			qty 		: qty2,
			amount		: amount,
			act 		: 'edit',
			type 		: 'qty',
		},
		headers: {
		    'Content-Type': 'application/x-www-form-urlencoded'
		},
		success: function(response) {
			$('#content').empty();
			get_data();
		},
	});
}

function delete_data(id){
    if (confirm("Do you want to delete this product form shopping cart?")) {
		$.ajax({
			type: 'POST',
			dataType: 'JSON',
			url: 'https://protegoindonesia.com/server/Cart/delete/_',
			data: {
				id 			: id,
			},
			headers: {
			    'Content-Type': 'application/x-www-form-urlencoded'
			},
			success: function(response) {
				$('#content').empty();
				get_data();
			},
		});
    }
}

function checkout(){
	var cart 		= {};
	cart.checked 	= [];
	cart.unchecked 	= [];
	var checked 	= 0;

	$("input[name='checkCart']").each(function() {
		if ($(this).is(':checked')) {
			var id = $(this).attr("id");
			cart.checked.push($(this).attr("id")+'|'+$('#remarks'+id).val());		
			checked += 1;	
		} 
	});

	if (checked == 0) {
		alert("Select items to checkout");
	} else {
		if ($('#address').val() == '' || $('#province').val() == '' || $('#city').val() == '' || $('#subDistrict').val() == '' || $('#zipCode').val() == '') {
			alert('Complete your address');
   			window.location.href = '../pages/account';
		} else {
		    if (confirm("Are you sure want to checkout shopping cart?")) {
				$.ajax({
					type: 'POST',
					dataType: 'JSON',
					url: 'https://protegoindonesia.com/server/Cart/addToPreorder/_',
					data: {
						id		: cart.checked,
					},
					headers: {
					    'Content-Type': 'application/x-www-form-urlencoded'
					},
					success: function(response) {
						if (response.status == true) {
							alert('Seller will contact you for your purchasing details.');
   							window.location.href = '../pages/cart';
						} else {
							alert('Checkout failed, try again later');
   							window.location.href = '../pages/cart';
						}
					},
				});
			}
		}
	}
}