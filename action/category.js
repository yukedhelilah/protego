$(document).ready(function() {
    $('.mobile-menu').css('color','#222');
    $('#products-menu').css('color','#9cc15c');
  	get_data();
    get_product();
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Category/get_all/_',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $.each(response.data, function( i, val ) {
        var txt = '';
      	txt +=`
          <div class="col-lg-2 col-md-3 col-ts-6" style="float:left">
            <div class="category" onclick="sub_category(`+val.id+`)">
              <h3 class="title" style="margin:0;padding:10px;margin-top:15px">
                <a class="font-mobile" tabindex="-1" style="color:white">`+ val.category +`</a>
              </h3>
              <div class="sub_category" id="sub_category`+val.id+`" style="display:none">`;

        $.each(val.sub_category, function( j, value ) {
          txt +=`
            <h3 class="title" style="margin:0;padding:10px;">
              <a href="products?search=`+btoa(value.id+`|`+val.category+`|`+value.sub_category+`|`+val.id)+`">
                <span class="font-mobile">`+ value.sub_category +`</span>
              </a>
            </h3>
          `;
        });

        txt +=`</div>
            </div>
          </div>
        `;

        $('#content').append(txt);
      });
    },
  });
}

function sub_category(id){
  $('#sub_category'+id).slideToggle();
}

function get_product(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Product/get_data_for_index/_',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $.each(response.data, function( i, val ) {
        var img = '';
        if (val.img1 == '') {
          img = `https://protegoindonesia.com/server/files/no-image.png`;
        } else {
          img = `https://protegoindonesia.com/server/files/product/`+val.img1;
        }
        $('#content-product').append(`
          <div class="col-md-2 col-ts-6">
                <div class="product-inner tooltip-top">
                    <div class="product-thumb">
                        <a class="thumb-link" href="products_details?search=`+btoa(val.id+`|`+val.category+`|`+val.sub_category+`|`+val.sub_categoryID+`|`+val.product+`|`+val.categoryID)+`" tabindex="0">
                            <img class="img-responsive" src="`+img+`" alt="Sweeper Funnel" width="100%" height="350">
                        </a>
                        <div class="flash">
                            <span class="onnew" style="float:left;margin-top:-22px"><span class="text">New</span></span>
                        </div>
                    </div>
                    <div class="product-info" style="margin-top:15px">
                        <h3 class="product-name product_title">
                            <a href="products_details?search=`+btoa(val.id+`|`+val.category+`|`+val.sub_category+`|`+val.sub_categoryID+`|`+val.product+`|`+val.categoryID)+`" tabindex="0">`+val.product+`</a>
                        </h3>
                        <span class="price">
                          <span class="furgan-Price-amount amount">
                            <span class="furgan-Price-currencySymbol">Rp </span>`+numberFormat(val.price)+`
                        </span>
                    </span>
                    </div>
                </div>
            </div>
        `);
      });
    },
  });
}