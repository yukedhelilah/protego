var buyerID     = atob(localStorage.getItem(btoa("buyer_id")));
var name     	= atob(localStorage.getItem(btoa("buyer_name")));
var username    = atob(localStorage.getItem(btoa("buyer_username")));
var token    	= atob(localStorage.getItem(btoa("buyer_token")));
var expired     = localStorage.getItem(btoa("buyer_expired"));
var exp         = new Date(atob(expired));
var now         = new Date();

var bulans          = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
var days            = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

if(name == null || username == null || token == null || expired == null || exp.getDate() +`-`+ exp.getMonth() +`-`+ exp.getFullYear() != now.getDate() +`-`+ now.getMonth() +`-`+ now.getFullYear()){
    removeLocalstorage();
    // window.location.href = '../pages/login';
    $('.openLogin').css('display','none');
    $('.notLogin').css('display','block');
} else {
    $('.openLogin').css('display','table-cell');
    $('.notLogin').css('display','none');
}

$(document).ready(function() {
    get_cart()
});

function dateFormat(date) {
    var format = new Date(date);
    var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();
    return date + ' ' + bulans[format.getMonth()] + ' ' + format.getFullYear().toString().substr(2,2);
}

function logout() {
    if (confirm("Do you want to logout?")) {
        removeLocalstorage();
        window.location.href = '../pages/home';
    } 
}

function removeLocalstorage(){
    localStorage.removeItem(btoa('buyer_id'));
    localStorage.removeItem(btoa('buyer_name'));
    localStorage.removeItem(btoa('buyer_username'));
    localStorage.removeItem(btoa('buyer_token'));
    localStorage.removeItem(btoa('buyer_expired'));
}

function get_cart(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Cart/get_total/_',
    data: {
        id : buyerID,
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
        $('#cart').html(response.data.total);
    },
  });
}