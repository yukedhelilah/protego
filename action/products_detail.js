var url     = atob(getUrlParameter('search')).split('|');

$(document).ready(function() {
    $('.mobile-menu').css('color','#222');
    $('#products-menu').css('color','#9cc15c');
  	get_data();

    $('#btn-img1').click(function() {
      $('.div-img').css('display','none');
      $('#div-img1').css('display','block');
      $('.icon-img').css('box-shadow','0px 0px 0px white');
      $('#btn-img1').css('box-shadow','2px 2px 7px #484848');
    });

    $('#btn-img2').click(function() {
      $('.div-img').css('display','none');
      $('#div-img2').css('display','block');
      $('.icon-img').css('box-shadow','0px 0px 0px white');
      $('#btn-img2').css('box-shadow','2px 2px 7px #484848');
    });

    $('#btn-img3').click(function() {
      $('.div-img').css('display','none');
      $('#div-img3').css('display','block');
      $('.icon-img').css('box-shadow','0px 0px 0px white');
      $('#btn-img3').css('box-shadow','2px 2px 7px #484848');
    });

    $('#btn-img4').click(function() {
      $('.div-img').css('display','none');
      $('#div-img4').css('display','block');
      $('.icon-img').css('box-shadow','0px 0px 0px white');
      $('#btn-img4').css('box-shadow','2px 2px 7px #484848');
    });

    $('#btn-img5').click(function() {
      $('.div-img').css('display','none');
      $('#div-img5').css('display','block');
      $('.icon-img').css('box-shadow','0px 0px 0px white');
      $('#btn-img5').css('box-shadow','2px 2px 7px #484848');
    });
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Product/get_details/_',
    data: {
    	id : url[0],
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
  	  $('#nav-category').html(response.data.category);
      $('#nav-sub-category').html(response.data.sub_category);
      $('#title').html(response.data.product);
      $('#link-sub-category').attr('href',`sub_category?search=`+btoa(response.data.categoryID+`|`+response.data.category));
      $('#link-product').attr('href',`products?search=`+btoa(response.data.id+`|`+response.data.category+`|`+response.data.sub_category+`|`+response.data.sub_categoryID));
      var img1 = response.data.img1 == '' ? `https://protegoindonesia.com/server/files/no-image.png` : `https://protegoindonesia.com/server/files/product/`+response.data.img1;
      var img2 = response.data.img2 == '' ? `https://protegoindonesia.com/server/files/no-image.png` : `https://protegoindonesia.com/server/files/product/`+response.data.img2;
      var img3 = response.data.img3 == '' ? `https://protegoindonesia.com/server/files/no-image.png` : `https://protegoindonesia.com/server/files/product/`+response.data.img3;
      var img4 = response.data.img4 == '' ? `https://protegoindonesia.com/server/files/no-image.png` : `https://protegoindonesia.com/server/files/product/`+response.data.img4;
      var img5 = response.data.img5 == '' ? `https://protegoindonesia.com/server/files/no-image.png` : `https://protegoindonesia.com/server/files/product/`+response.data.img5;
      
      $('#img1, #icon-img1').attr('src',img1);
      $('#img2, #icon-img2').attr('src',img2);
      $('#img3, #icon-img3').attr('src',img3);
      $('#img4, #icon-img4').attr('src',img4);
      $('#img5, #icon-img5').attr('src',img5);

      $('#div-qty').append(`
        <div onclick="qtyMin(`+response.data.id +`,`+ $('#qty').val() +`,`+ response.data.price+`)" class="btn-solid btn-numeric" style="line-height: 0.82;font-size: 26px;top: 50%;transform: translateY(-50%);position: relative;cursor:pointer">_</div>
        <div id="jumlah" class="btn-solid" style="border-right: 1px solid white;border-left: 1px solid white;font-size: 20px;line-height: 1.8;width: 60px;top: 50%;transform: translateY(-50%);position: relative;">`+ $('#qty').val() +`</div>
        <div onclick="qtyPlus(`+response.data.id +`,`+ $('#qty').val() +`,`+ response.data.price+`)" class="btn-solid btn-numeric" style="top: 50%;transform: translateY(-50%);position: relative;cursor:pointer">+</div>
      
        <div class="btn-green2" style="float:right;width: auto;padding: 4px 10px;height: auto;margin-top: -18px;" onclick="addToChart(`+response.data.id +`,`+ $('#qty').val() +`,`+ response.data.price+`)"><i class="fa fa-shopping-cart"></i> Add to Cart</div>
      `);

      $('#product').html(response.data.product);
      $('#price').html('Rp '+numberFormat(response.data.price));
      $('#description').html(response.data.description);

      var blibli    = response.data.blibli != '' ? `<br>Blibli : <a href="`+response.data.blibli+`" target="_blank">` + response.data.blibli + `</a>` : '';
      var bukalapak = response.data.bukalapak != '' ? `<br>Bukalapak : <a href="`+response.data.bukalapak+`" target="_blank">` + response.data.bukalapak + `</a>` : '';
      var lazada    = response.data.lazada != '' ? `<br>Lazada : <a href="`+response.data.lazada+`" target="_blank">` + response.data.lazada + `</a>` : '';
      var shopee    = response.data.shopee != '' ? `<br>Shopee : <a href="`+response.data.shopee+`" target="_blank">` + response.data.shopee + `</a>` : '';
      var tokopedia = response.data.tokopedia != '' ? `<br>Tokopedia : <a href="`+response.data.tokopedia+`" target="_blank">` + response.data.tokopedia + `</a>` : '';
      var lainnya   = response.data.lainnya != '' ? `<br>Lainnya : <a href="`+response.data.lainnya+`" target="_blank">` + response.data.lainnya + `</a>` : '';
      var lainnya2  = response.data.lainnya2 != '' ? `<br>Lainnya : <a href="`+response.data.lainnya2+`" target="_blank">` + response.data.lainnya2 + `</a>` : ''; 

      $('#contact').html(`
        <h4 class="title style="font-size:12px">Info Seller</h4>
        <div class="desc" style="font-size:12px">
          <a href="store?search=`+btoa(response.data.sellerID+`|`+response.data.name)+`"><b>`+response.data.name+`</b></a><br>
          `+response.data.email+`<br>
          `+response.data.mobile+`
          `+blibli+`
          `+bukalapak+`
          `+lazada+`
          `+shopee+`
          `+tokopedia+`
          `+lainnya+`
          `+lainnya2+`
        </div>
      `);
    },
  });
}

function qtyMin(productID, qty, amount){
  if (qty == 1) {
    alert('Minimum order 1pcs');
  } else {
    qty2        = qty - 1; 
    $('#jumlah').html(qty2);
    $('#qty').val(qty2);
  }

  $('#div-qty').empty();
  $('#div-qty').append(`
    <div onclick="qtyMin(`+productID +`,`+ $('#qty').val() +`,`+ amount+`)" class="btn-solid btn-numeric" style="line-height: 0.82;font-size: 26px;top: 50%;transform: translateY(-50%);position: relative;cursor:pointer">_</div>
    <div id="jumlah" class="btn-solid" style="border-right: 1px solid white;border-left: 1px solid white;font-size: 20px;line-height: 1.8;width: 60px;top: 50%;transform: translateY(-50%);position: relative;">`+ $('#qty').val() +`</div>
    <div onclick="qtyPlus(`+productID +`,`+ $('#qty').val() +`,`+ amount+`)" class="btn-solid btn-numeric" style="top: 50%;transform: translateY(-50%);position: relative;cursor:pointer">+</div>
  
    <div class="btn-green2" style="float:right;width: auto;padding: 4px 10px;height: auto;margin-top: -18px;" onclick="addToChart(`+productID +`,`+ $('#qty').val() +`,`+ amount+`)"><i class="fa fa-shopping-cart"></i> Add to Cart</div>
  `);
}

function qtyPlus(productID, qty, amount){
  qty2    = qty + 1; 
  $('#jumlah').html(qty2);
  $('#qty').val(qty2);

  $('#div-qty').empty();
  $('#div-qty').append(`
    <div onclick="qtyMin(`+productID +`,`+ $('#qty').val() +`,`+ amount+`)" class="btn-solid btn-numeric" style="line-height: 0.82;font-size: 26px;top: 50%;transform: translateY(-50%);position: relative;cursor:pointer">_</div>
    <div id="jumlah" class="btn-solid" style="border-right: 1px solid white;border-left: 1px solid white;font-size: 20px;line-height: 1.8;width: 60px;top: 50%;transform: translateY(-50%);position: relative;">`+ $('#qty').val() +`</div>
    <div onclick="qtyPlus(`+productID +`,`+ $('#qty').val() +`,`+ amount+`)" class="btn-solid btn-numeric" style="top: 50%;transform: translateY(-50%);position: relative;cursor:pointer">+</div>
  
    <div class="btn-green2" style="float:right;width: auto;padding: 4px 10px;height: auto;margin-top: -18px;" onclick="addToChart(`+productID +`,`+ $('#qty').val() +`,`+ amount+`)"><i class="fa fa-shopping-cart"></i> Add to Cart</div>
  `);
}

function addToChart(productID, qty, amount){
  if (localStorage.getItem(btoa("buyer_id")) == null) {
    alert('You must login to add this product to cart');
    window.location.href = '../pages/login';
  } else {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: 'https://protegoindonesia.com/server/Cart/save/_',
      data: {
        act         : 'add',
        buyerID     : buyerID,
        productID   : productID,
        qty         : qty,
        amount      : amount,
        totalAmount : amount*qty,
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        if (response.code == 200) {
          window.location.href = '../pages/cart';
        } else {
          alert('Failed, try again later');
        }
      },
    });
  }
}