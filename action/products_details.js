var url     = atob(getUrlParameter('search')).split('|');

$(document).ready(function() {
    $('.mobile-menu').css('color','#222');
    $('#products-menu').css('color','#9cc15c');
  	get_data();

    $('#btn-img1').click(function() {
      $('.div-img').css('display','none');
      $('#div-img1').css('display','block');
      $('.icon-img').css('box-shadow','0px 0px 0px white');
      $('#btn-img1').css('box-shadow','2px 2px 7px #484848');
    });

    $('#btn-img2').click(function() {
      $('.div-img').css('display','none');
      $('#div-img2').css('display','block');
      $('.icon-img').css('box-shadow','0px 0px 0px white');
      $('#btn-img2').css('box-shadow','2px 2px 7px #484848');
    });

    $('#btn-img3').click(function() {
      $('.div-img').css('display','none');
      $('#div-img3').css('display','block');
      $('.icon-img').css('box-shadow','0px 0px 0px white');
      $('#btn-img3').css('box-shadow','2px 2px 7px #484848');
    });

    $('#btn-img4').click(function() {
      $('.div-img').css('display','none');
      $('#div-img4').css('display','block');
      $('.icon-img').css('box-shadow','0px 0px 0px white');
      $('#btn-img4').css('box-shadow','2px 2px 7px #484848');
    });

    $('#btn-img5').click(function() {
      $('.div-img').css('display','none');
      $('#div-img5').css('display','block');
      $('.icon-img').css('box-shadow','0px 0px 0px white');
      $('#btn-img5').css('box-shadow','2px 2px 7px #484848');
    });
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Product/get_details_by_product/_',
    data: {
    	product : url[4],
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $('#contact').append(`<h4 class="title style="font-size:12px">Info Seller</h4>`);
      $.each(response.data, function( i, val ) {
    	  $('#nav-category').html(val.category);
        $('#nav-sub-category').html(val.sub_category);
        $('#title').html(val.product);
        $('#link-sub-category').attr('href',`sub_category?search=`+btoa(val.categoryID+`|`+val.category));
        $('#link-product').attr('href',`products?search=`+btoa(val.id+`|`+val.category+`|`+val.sub_category+`|`+val.sub_categoryID));
        var img1 = val.img1 == '' ? `https://protegoindonesia.com/server/files/no-image.png` : `https://protegoindonesia.com/server/files/product/`+val.img1;
        var img2 = val.img2 == '' ? `https://protegoindonesia.com/server/files/no-image.png` : `https://protegoindonesia.com/server/files/product/`+val.img2;
        var img3 = val.img3 == '' ? `https://protegoindonesia.com/server/files/no-image.png` : `https://protegoindonesia.com/server/files/product/`+val.img3;
        var img4 = val.img4 == '' ? `https://protegoindonesia.com/server/files/no-image.png` : `https://protegoindonesia.com/server/files/product/`+val.img4;
        var img5 = val.img5 == '' ? `https://protegoindonesia.com/server/files/no-image.png` : `https://protegoindonesia.com/server/files/product/`+val.img5;
        
        $('#img1, #icon-img1').attr('src',img1);
        $('#img2, #icon-img2').attr('src',img2);
        $('#img3, #icon-img3').attr('src',img3);
        $('#img4, #icon-img4').attr('src',img4);
        $('#img5, #icon-img5').attr('src',img5);

        $('#product').html(val.product);
        $('#price').html('Rp '+numberFormat(val.price));
        $('#description').html(val.description);

        var blibli    = val.blibli != '' ? `<br>Blibli : <a href="`+val.blibli+`" target="_blank">` + val.blibli + `</a>` : '';
        var bukalapak = val.bukalapak != '' ? `<br>Bukalapak : <a href="`+val.bukalapak+`" target="_blank">` + val.bukalapak + `</a>` : '';
        var lazada    = val.lazada != '' ? `<br>Lazada : <a href="`+val.lazada+`" target="_blank">` + val.lazada + `</a>` : '';
        var shopee    = val.shopee != '' ? `<br>Shopee : <a href="`+val.shopee+`" target="_blank">` + val.shopee + `</a>` : '';
        var tokopedia = val.tokopedia != '' ? `<br>Tokopedia : <a href="`+val.tokopedia+`" target="_blank">` + val.tokopedia + `</a>` : '';
        var lainnya   = val.lainnya != '' ? `<br>Lainnya : <a href="`+val.lainnya+`" target="_blank">` + val.lainnya + `</a>` : '';
        var lainnya2  = val.lainnya2 != '' ? `<br>Lainnya : <a href="`+val.lainnya2+`" target="_blank">` + val.lainnya2 + `</a>` : ''; 

        $('#contact').append(`
          <hr>
          <div class="desc" style="font-size:12px">
            <a href="store?search=`+btoa(val.sellerID+`|`+val.name)+`"><b style="color:black">`+val.name+`</b></a><br>
            `+val.email+`<br>
            `+val.mobile+`
            `+blibli+`
            `+bukalapak+`
            `+lazada+`
            `+shopee+`
            `+tokopedia+`
            `+lainnya+`
            `+lainnya2+`
          </div>
        `);
      });

    },
  });
}