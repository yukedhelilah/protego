var url     = atob(getUrlParameter('search'));

$(function(){
  $('.mobile-menu').css('color','#222');
  $('#store-menu').css('color','#9cc15c');
  
  setTimeout(function () {
    get_data();
  }, 500);
  
  setTimeout(function () {
    category();
    sub_category($('#category').val());
  }, 1500);

  $("#categoryID").change(function () {
    sub_category($(this).val());
  });

  $(".editor").each(function () {
    let id = $(this).attr('id');
    CKEDITOR.replace(id);
  });

  $("#file_img1").change(function () {
      uploadImg(1);
  });

  $("#file_img2").change(function () {
      uploadImg(2);
  });

  $("#file_img3").change(function () {
      uploadImg(3);
  }); 

  $("#file_img4").change(function () {
      uploadImg(4);
  });

  $("#file_img5").change(function () {
      uploadImg(5);
  }); 

  setTimeout(function () {
    loadImg();
  }, 4000);
});

function category(){
  $('#categoryID').empty();

  $.ajax({
    type: 'POST',
    url: 'https://protegoindonesia.com/server/Category/get_data/_',
    dataType: 'JSON',
    success: function(responses) {
      $.each(responses.data, function(i, e){
        if (e.id == $('#category').val()) {
          $('#categoryID').append('<option value="'+ e.id +'" selected>'+ e.category +'</option>');
        } else {
          $('#categoryID').append('<option value="'+ e.id +'">'+ e.category +'</option>');
        }
      });
    }
  });
}

function sub_category(id){
  $('#sub_categoryID').empty();

  $.ajax({
    type: 'POST',
    url: 'https://protegoindonesia.com/server/Sub_category/get_data_by_category/_',
    data: {
      id : id,
    },
    dataType: 'JSON',
    success: function(responses) {
      if (responses.data != null) {
        $.each(responses.data, function(i, e){
          if (e.id == $('#sub_category').val()) {
            $('#sub_categoryID').append('<option value="'+ e.id +'" selected>'+ e.sub_category +'</option>');
          } else {
            $('#sub_categoryID').append('<option value="'+ e.id +'">'+ e.sub_category +'</option>');
          }
        });
      } else {
        $('#sub_categoryID').append('<option>--- sub category not available ---</option>');
      }
    }
  });
}

function get_data(){
  $.ajax({
    type: 'POST',
    url: 'https://protegoindonesia.com/server/Product/get_details/_',
    dataType: 'JSON',
    data : {
      id : url,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(responses) {
      $('#id').val(responses.data.id);
      $('#categoryID').val(responses.data.categoryID);
      $('#category').val(responses.data.categoryID);
      $('#sub_categoryID').val(responses.data.sub_categoryID);
      $('#sub_category').val(responses.data.sub_categoryID);
      $('#product').val(responses.data.product);
      $('#price').val(responses.data.price);
      CKEDITOR.instances['description'].setData(responses.data.description);
      $('#img1').val(responses.data.img1);
      $('#img2').val(responses.data.img2);
      $('#img3').val(responses.data.img3);
      $('#img4').val(responses.data.img4);
      $('#img5').val(responses.data.img5);
      $('#isCopy').val(responses.data.id);
      $('#copy').html(`( Original Product by <span style="color:red">`+responses.data.name+`</span> )`);
    }
  });
}

function uploadImg(no){
    if (($("#file_img"+no))[0].files.length > 0) {
        var file = $("#file_img"+no)[0].files[0];
        var formdata = new FormData();
        formdata.append("file_img", file);
        formdata.append("no", no);
        var ajax = new XMLHttpRequest();
        ajax.addEventListener("load", completeHandler2, false);
        ajax.open("POST", "https://protegoindonesia.com/server/Product/uploadImg/_");
        ajax.send(formdata);
    } else {
        alert("No file chosen!");
    }
}

function completeHandler2(event){
    var data = event.target.responseText.split('*');
    var no = data[2];
    if(data[0]!=''){
        $('#file_img'+no).val('');
        alert("Error! "+ data[1]);
    }else{
        $('#img'+no).val(data[1]);
        loadImg();
        $('#file_img'+no).val('');
    }   
}

function loadImg(){
    if($('#img1').val() == ''){
      $("#img_product1").attr("src","https://protegoindonesia.com/server/files/no-image.png");
      $('#txt_img1').html('');
    }else{
      $('#txt_img1').html(`
        <div style="width:50%;text-align:center;float:left">
          <a href="https://protegoindonesia.com/server/files/product/`+ $('#img1').val() +`" 
          class="tx-12 tx-medium tx-indigo" target="_blank" >
            <i style="color:green" class="fa fa-search mg-l-5"></i></a>
        </div>
        <div style="width:50%;text-align:center;float:right">
          <a href="javascript:;" onclick="removeImg(1)" class="tx-12 tx-medium tx-danger">
            <i style="color:red" class="fa fa-trash mg-l-5"></i></a>
        </div>
      `);
      $("#img_product1").attr("src","https://protegoindonesia.com/server/files/product/"+$('#img1').val());
    } 

    if($('#img2').val() == ''){
      $("#img_product2").attr("src","https://protegoindonesia.com/server/files/no-image.png");
      $('#txt_img2').html('');
    }else{
      $('#txt_img2').html(`
        <div style="width:50%;text-align:center;float:left">
          <a href="https://protegoindonesia.com/server/files/product/`+ $('#img2').val() +`" 
          class="tx-12 tx-medium tx-indigo" target="_blank" >
            <i style="color:green" class="fa fa-search mg-l-5"></i></a>
        </div>
        <div style="width:50%;text-align:center;float:right">
          <a href="javascript:;" onclick="removeImg(2)" class="tx-12 tx-medium tx-danger">
            <i style="color:red" class="fa fa-trash mg-l-5"></i></a>
        </div>
      `);
      $("#img_product2").attr("src","https://protegoindonesia.com/server/files/product/"+$('#img2').val());
    } 

    if($('#img3').val() == ''){
      $("#img_product3").attr("src","https://protegoindonesia.com/server/files/no-image.png");
      $('#txt_img3').html('');
    }else{
      $('#txt_img3').html(`
        <div style="width:50%;text-align:center;float:left">
          <a href="https://protegoindonesia.com/server/files/product/`+ $('#img3').val() +`" 
          class="tx-12 tx-medium tx-indigo" target="_blank" >
            <i style="color:green" class="fa fa-search mg-l-5"></i></a>
        </div>
        <div style="width:50%;text-align:center;float:right">
          <a href="javascript:;" onclick="removeImg(3)" class="tx-12 tx-medium tx-danger">
            <i style="color:red" class="fa fa-trash mg-l-5"></i></a>
        </div>
      `);
      $("#img_product3").attr("src","https://protegoindonesia.com/server/files/product/"+$('#img3').val());
    }       

    if($('#img4').val() == ''){
      $("#img_product4").attr("src","https://protegoindonesia.com/server/files/no-image.png");
      $('#txt_img4').html('');
    }else{
      $('#txt_img4').html(`
        <div style="width:50%;text-align:center;float:left">
          <a href="https://protegoindonesia.com/server/files/product/`+ $('#img4').val() +`" 
          class="tx-12 tx-medium tx-indigo" target="_blank" >
            <i style="color:green" class="fa fa-search mg-l-5"></i></a>
        </div>
        <div style="width:50%;text-align:center;float:right">
          <a href="javascript:;" onclick="removeImg(4)" class="tx-12 tx-medium tx-danger">
            <i style="color:red" class="fa fa-trash mg-l-5"></i></a>
        </div>
      `);
      $("#img_product4").attr("src","https://protegoindonesia.com/server/files/product/"+$('#img4').val());
    } 

    if($('#img5').val() == ''){
      $("#img_product5").attr("src","https://protegoindonesia.com/server/files/no-image.png");
      $('#txt_img5').html('');
    }else{
      $('#txt_img5').html(`
        <div style="width:50%;text-align:center;float:left">
          <a href="https://protegoindonesia.com/server/files/product/`+ $('#img5').val() +`" 
          class="tx-12 tx-medium tx-indigo" target="_blank" >
            <i style="color:green" class="fa fa-search mg-l-5"></i></a>
        </div>
        <div style="width:50%;text-align:center;float:right">
          <a href="javascript:;" onclick="removeImg(5)" class="tx-12 tx-medium tx-danger">
            <i style="color:red" class="fa fa-trash mg-l-5"></i></a>
        </div>
      `);
      $("#img_product5").attr("src","https://protegoindonesia.com/server/files/product/"+$('#img5').val());
    }    
}

function removeImg(no){
  $('#img'+no).val('');
  loadImg();
}

function save(){
  for (instance in CKEDITOR.instances){
    CKEDITOR.instances[instance].updateElement();
  }
  
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Product/save/_',
    data: $('#form_master').serialize()+"&sellerID="+sellerID,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.code == 200) {
        window.location.href = '../pages/store';
      } else {
        alert('failed, try again later.');
      }
    },
  });
}