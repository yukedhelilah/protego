$(document).ready(function() {
    $('.mobile-menu').css('color','#222');
    $('#store-menu').css('color','#9cc15c');
  	get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Product/get_data_by_seller/_',
    data: {
    	id : sellerID,
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $.each(response.data, function( i, val ) {
      	var img = '';
      	if (val.img1 == '') {
      		img = `https://protegoindonesia.com/server/files/no-image.png`;
      	} else {
      		img = `https://protegoindonesia.com/server/files/product/`+val.img1;
      	}
      	$('#content').append(`
            <div class="col-ts-6 col-md-2" style="margin:15px 0;">
                <div class="product-inner tooltip-top tooltip-all-top">
                    <div class="product-thumb">
                        <a class="thumb-link" href="edit_product?search=`+btoa(val.id)+`" tabindex="0">
                            <img class="img-responsive" src="`+img+`" alt="`+val.product+`" width="100%" height="350">
                        </a>
                        <div onclick="view_data('`+val.id+`','`+val.category+`','`+val.sub_category+`','`+val.sub_categoryID+`','`+val.product+`','`+val.categoryID+`')" class="btn-green2" style="text-align:center;width:23.33%;margin:5% 5% 15px 5%;float:left"><i class="fa fa-eye"></i></div>
                        <div onclick="edit_data(`+val.id+`)" class="btn-blue" style="text-align:center;width:23.33%;margin:5% 5% 15px 5%;float:left"><i class="fa fa-pencil"></i></div>
                        <div onclick="delete_data(`+val.id+`)" class="btn-red" style="text-align:center;width:23.33%;margin:5% 5% 15px 5%;float:left"><i class="fa fa-trash"></i></div>
                    </div>
                    <div class="product-info" style="text-align: center;padding-top: 15px">
                        <h3 class="product-name product_title">
                            <a href="edit_product?search=`+btoa(val.id)+`" tabindex="0">`+val.product+`
                           </a>
                        </h3>
                        <span class="price">
                            <span class="furgan-Price-amount amount">
                                <span class="furgan-Price-currencySymbol">Rp </span> `+numberFormat(val.price)+`
                            </span>
                        </span>
                    </div>
                </div>
            </div>
        `);
      });
    },
  });
}

function view_data(id, category, sub_category, sub_categoryID, product, categoryID){
  window.location.href = `products_detail?search=`+btoa(id+`|`+category+`|`+sub_category+`|`+sub_categoryID+`|`+product+`|`+categoryID);
}

function edit_data(id){
  window.location.href = `edit_product?search=`+btoa(id);
}

function delete_data(id){
  if (confirm("Do you want to delete this product?")) {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: 'https://protegoindonesia.com/server/Product/delete/_',
      data: {
        id : id,
      },
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        $('#content').empty();
        get_data();
      },
    });
  }
}