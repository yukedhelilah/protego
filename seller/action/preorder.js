$(document).ready(function() {
    $('.mobile-menu').css('color','#222');
    $('#preorder-menu').css('color','#9cc15c');
  	get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Preorder/get_data_bysellerID/_',
    data: {
    	id : sellerID,
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $.each(response.data, function( i, val ) {
        var now = new Date();
        now.setDate(now.getDate() + 2); 
        var status = val.status == 0 ? '<i class="fa fa-clock-o" style="font-size:18px;color:orange"></i>' : '<i class="fa fa-check-circle-o" style="font-size:18px;color:green"></i>';
      	$('#tb_preorder').append(`
          <tr>
            <td>`+ val.product +`<br></td>
            <td style="text-align:center">`+ val.qty +`</td>
            <td style="text-align:center">`+ numberFormat(val.amount * val.qty) +`</td>
            <td style="text-align:center">`+ dateFormat2(val.deadline) +`</td>
            <td>`+ val.province +`, `+ val.city +`, `+ val.subDistrict +`, `+ val.address +`, `+ val.zipCode +`</td>
            <td style="text-align:center">`+ status +`</td>
            <td style="text-align:center">
              <a style="font-size:18px;color:blue" href="edit_preorder?search=`+btoa(val.id)+`"><i class="fa fa-edit"></i></a>
              <a style="font-size:18px;color:green" target="_blank" href="https://api.whatsapp.com/send?phone=`+val.mobile+`&text=Product%20:%20`+val.product+`%0AVarian%20:%20`+val.type+`%0AJumlah%20:%20`+val.qty+`%0AHarga/pcs%20:%20`+numberFormat(val.amount)+`%0AOngkir%20:%0ATotal%20:%20`+numberFormat(val.qty * val.amount)+`%0A%0AMohon%20konfirmasi%20pembayaran%20sebelum%20`+dateFormat(now)+`"><i class="fa fa-whatsapp"></i></a>
            </td>
          </tr>
        `);
      });
    },
  });
}