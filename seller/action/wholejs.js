var sellerID    = atob(localStorage.getItem(btoa("seller_id")));
var name     	= atob(localStorage.getItem(btoa("seller_name")));
var username    = atob(localStorage.getItem(btoa("seller_username")));
var token    	= atob(localStorage.getItem(btoa("seller_token")));
var expired     = localStorage.getItem(btoa("seller_expired"));
var exp         = new Date(atob(expired));
var now         = new Date();

var bulans          = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
var days            = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

if(name == null || username == null || token == null || expired == null || exp.getDate() +`-`+ exp.getMonth() +`-`+ exp.getFullYear() != now.getDate() +`-`+ now.getMonth() +`-`+ now.getFullYear()){
    removeLocalstorage();
    window.location.href = '../pages/login';
}

$(document).ready(function() {
});

function dateFormat(date) {
    var format = new Date(date);
    var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();
    return date + ' ' + bulans[format.getMonth()] + ' ' + format.getFullYear().toString();
}

function dateFormat2(date) {
    var format = new Date(date);
    var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();
    return date + ' ' + bulans[format.getMonth()];
}

function logout() {
    if (confirm("Do you want to logout?")) {
        removeLocalstorage();
        window.location.href = '../pages/login';
    } 
}

function removeLocalstorage(){
    localStorage.removeItem(btoa('seller_id'));
    localStorage.removeItem(btoa('seller_name'));
    localStorage.removeItem(btoa('seller_username'));
    localStorage.removeItem(btoa('seller_token'));
    localStorage.removeItem(btoa('seller_expired'));
}