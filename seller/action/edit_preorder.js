var url     = atob(getUrlParameter('search'));

$(function(){
  $('.mobile-menu').css('color','#222');
    $('#preorder-menu').css('color','#9cc15c');
  
  get_data();
  setTimeout(function () {
    category();
    sub_category($('#category').val());
    product($('#sub_category').val());
  }, 1000);

  $("#categoryID").change(function () {
    sub_category($(this).val());
  });

  $("#sub_categoryID").change(function () {
    product($(this).val());
  });

  $(".editor").each(function () {
    let id = $(this).attr('id');
    CKEDITOR.replace(id);
  });
});

function category(){
  $('#categoryID').empty();

  $.ajax({
    type: 'POST',
    url: 'https://protegoindonesia.com/server/Category/get_data/_',
    dataType: 'JSON',
    success: function(responses) {
      $.each(responses.data, function(i, e){
        if (e.id == $('#category').val()) {
          $('#categoryID').append('<option value="'+ e.id +'" selected>'+ e.category +'</option>');
        } else {
          $('#categoryID').append('<option value="'+ e.id +'">'+ e.category +'</option>');
        }
      });
    }
  });
}

function sub_category(id){
  $('#sub_categoryID').empty();

  $.ajax({
    type: 'POST',
    url: 'https://protegoindonesia.com/server/Sub_category/get_data_by_category/_',
    data: {
      id : id,
    },
    dataType: 'JSON',
    success: function(responses) {
      if (responses.data != null) {
        $.each(responses.data, function(i, e){
          if (e.id == $('#sub_category').val()) {
            $('#sub_categoryID').append('<option value="'+ e.id +'" selected>'+ e.sub_category +'</option>');
          } else {
            $('#sub_categoryID').append('<option value="'+ e.id +'">'+ e.sub_category +'</option>');
          }
        });
      } else {
        $('#sub_categoryID').append('<option>--- sub category not available ---</option>');
      }
    }
  });
}

function product(id){
  $('#productID').empty();

  $.ajax({
    type: 'POST',
    url: 'https://protegoindonesia.com/server/Product/get_data_by_sub_category/_',
    data: {
      id : id,
    },
    dataType: 'JSON',
    success: function(responses) {
      if (responses.data != null) {
        $.each(responses.data, function(i, e){
          if (e.id == $('#product').val()) {
            $('#productID').append('<option value="'+ e.id +'" selected>'+ e.product +'</option>');
          } else {
            $('#productID').append('<option value="'+ e.id +'">'+ e.product +'</option>');
          }
        });
      } else {
        $('#productID').append('<option>--- product not available ---</option>');
      }
    }
  });
}

function get_data(){
  $.ajax({
    type: 'POST',
    url: 'https://protegoindonesia.com/server/Preorder/get_details/_',
    dataType: 'JSON',
    data : {
      id : url,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(responses) {
      $('#id').val(responses.data.id);
      $('#categoryID').val(responses.data.categoryID);
      $('#category').val(responses.data.categoryID);
      $('#sub_categoryID').val(responses.data.sub_categoryID);
      $('#sub_category').val(responses.data.sub_categoryID);
      $('#productID').val(responses.data.productID);
      $('#product').val(responses.data.productID);
      $('#qty').val(responses.data.qty);
      $('#type').val(responses.data.type);
      $('#deadline').val(dateFormat(responses.data.deadline));
      CKEDITOR.instances['remarks'].setData(responses.data.remarks);
      $('#status').val(responses.data.status);
    }
  });
}

function save(){
  for (instance in CKEDITOR.instances){
    CKEDITOR.instances[instance].updateElement();
  }

  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Preorder/save/_',
    data: $('#form_master').serialize()+"&sellerID="+sellerID,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.code == 200) {
        window.location.href = '../pages/preorder';
      } else {
        alert('failed, try again later.');
      }
    },
  });
}