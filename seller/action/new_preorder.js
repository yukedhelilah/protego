$(function(){
    $('.mobile-menu').css('color','#222');
    $('#preorder-menu').css('color','#9cc15c');
    
    category();
    $("#categoryID").change(function () {
        sub_category($(this).val());
    });

    $("#sub_categoryID").change(function () {
        product($(this).val());
    });

    $(".editor").each(function () {
        let id = $(this).attr('id');
        CKEDITOR.replace(id);
    });
});

function category(){
  $('#categoryID').empty();

  $.ajax({
    type: 'POST',
    url: 'https://protegoindonesia.com/server/Category/get_data/_',
    dataType: 'JSON',
    success: function(responses) {
      $.each(responses.data, function(i, e){
          if (i == 0) {
            $('#categoryID').append('<option value="'+ e.id +'" selected>'+ e.category +'</option>');
            sub_category(e.id);
          } else {
            $('#categoryID').append('<option value="'+ e.id +'">'+ e.category +'</option>');
        }
      });
    }
  });
}
  
function sub_category(id){
  $('#sub_categoryID').empty();

  $.ajax({
    type: 'POST',
    url: 'https://protegoindonesia.com/server/Sub_category/get_data_by_category/_',
    data: {
      id : id,
    },
    dataType: 'JSON',
    success: function(responses) {
      if (responses.data != null) {
        $.each(responses.data, function(i, e){
          if (i == 0) {
            $('#sub_categoryID').append('<option value="'+ e.id +'" selected>'+ e.sub_category +'</option>');
            product(e.id);
          } else {
            $('#sub_categoryID').append('<option value="'+ e.id +'">'+ e.sub_category +'</option>');
          }
        });
      } else {
        $('#sub_categoryID').append('<option>--- sub category not available ---</option>');
      }
    }
  });
}

function product(id){
  $('#productID').empty();

  $.ajax({
    type: 'POST',
    url: 'https://protegoindonesia.com/server/Product/get_data_by_sub_category/_',
    data: {
      id : id,
    },
    dataType: 'JSON',
    success: function(responses) {
      if (responses.data != null) {
        $.each(responses.data, function(i, e){
          $('#productID').append('<option value="'+ e.id +'" selected>'+ e.product +'</option>');
        });
      } else {
        $('#productID').append('<option>--- product not available ---</option>');
      }
    }
  });
}

function save(){
  for (instance in CKEDITOR.instances){
    CKEDITOR.instances[instance].updateElement();
  }
  
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Preorder/save/_',
    data: $('#form_master').serialize()+"&sellerID="+sellerID,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.code == 200) {
        window.location.href = '../pages/preorder';
      } else {
        alert('failed, try again later.');
      }
    },
  });
}

