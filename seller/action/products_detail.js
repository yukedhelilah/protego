var url     = atob(getUrlParameter('search')).split('|');

$(document).ready(function() {
    $('.mobile-menu').css('color','#222');
    $('#home-menu').css('color','#9cc15c');
  	get_data();

    $('#btn-img1').click(function() {
      $('.div-img').css('display','none');
      $('#div-img1').css('display','block');
      $('.icon-img').css('box-shadow','0px 0px 0px white');
      $('#btn-img1').css('box-shadow','2px 2px 7px #484848');
    });

    $('#btn-img2').click(function() {
      $('.div-img').css('display','none');
      $('#div-img2').css('display','block');
      $('.icon-img').css('box-shadow','0px 0px 0px white');
      $('#btn-img2').css('box-shadow','2px 2px 7px #484848');
    });

    $('#btn-img3').click(function() {
      $('.div-img').css('display','none');
      $('#div-img3').css('display','block');
      $('.icon-img').css('box-shadow','0px 0px 0px white');
      $('#btn-img3').css('box-shadow','2px 2px 7px #484848');
    });

    $('#btn-img4').click(function() {
      $('.div-img').css('display','none');
      $('#div-img4').css('display','block');
      $('.icon-img').css('box-shadow','0px 0px 0px white');
      $('#btn-img4').css('box-shadow','2px 2px 7px #484848');
    });

    $('#btn-img5').click(function() {
      $('.div-img').css('display','none');
      $('#div-img5').css('display','block');
      $('.icon-img').css('box-shadow','0px 0px 0px white');
      $('#btn-img5').css('box-shadow','2px 2px 7px #484848');
    });
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Product/get_details/_',
    data: {
    	id : url[0],
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $('#title, #nav-product').html(response.data.product);
      var img1 = response.data.img1 == '' ? `https://protegoindonesia.com/server/files/no-image.png` : `https://protegoindonesia.com/server/files/product/`+response.data.img1;
      var img2 = response.data.img2 == '' ? `https://protegoindonesia.com/server/files/no-image.png` : `https://protegoindonesia.com/server/files/product/`+response.data.img2;
      var img3 = response.data.img3 == '' ? `https://protegoindonesia.com/server/files/no-image.png` : `https://protegoindonesia.com/server/files/product/`+response.data.img3;
      var img4 = response.data.img4 == '' ? `https://protegoindonesia.com/server/files/no-image.png` : `https://protegoindonesia.com/server/files/product/`+response.data.img4;
      var img5 = response.data.img5 == '' ? `https://protegoindonesia.com/server/files/no-image.png` : `https://protegoindonesia.com/server/files/product/`+response.data.img5;
      
      $('#img1, #icon-img1').attr('src',img1);
      $('#img2, #icon-img2').attr('src',img2);
      $('#img3, #icon-img3').attr('src',img3);
      $('#img4, #icon-img4').attr('src',img4);
      $('#img5, #icon-img5').attr('src',img5);

      $('#product').html(response.data.product);
      $('#price').html('Rp '+numberFormat(response.data.price));
      $('#description').html(response.data.description);

      var blibli    = response.data.blibli != '' ? `<br>Blibli : <a href="`+response.data.blibli+`" target="_blank">` + response.data.blibli + `</a>` : '';
      var bukalapak = response.data.bukalapak != '' ? `<br>Bukalapak : <a href="`+response.data.bukalapak+`" target="_blank">` + response.data.bukalapak + `</a>` : '';
      var lazada    = response.data.lazada != '' ? `<br>Lazada : <a href="`+response.data.lazada+`" target="_blank">` + response.data.lazada + `</a>` : '';
      var shopee    = response.data.shopee != '' ? `<br>Shopee : <a href="`+response.data.shopee+`" target="_blank">` + response.data.shopee + `</a>` : '';
      var tokopedia = response.data.tokopedia != '' ? `<br>Tokopedia : <a href="`+response.data.tokopedia+`" target="_blank">` + response.data.tokopedia + `</a>` : '';
      var lainnya   = response.data.lainnya != '' ? `<br>Lainnya : <a href="`+response.data.lainnya+`" target="_blank">` + response.data.lainnya + `</a>` : '';
      var lainnya2  = response.data.lainnya2 != '' ? `<br>Lainnya : <a href="`+response.data.lainnya2+`" target="_blank">` + response.data.lainnya2 + `</a>` : ''; 

      $('#contact').html(`
        <h4 class="title style="font-size:12px">Info Seller</h4>
        <div class="desc" style="font-size:12px">
          `+response.data.name+`<br>
          `+response.data.email+`<br>
          `+response.data.mobile+`
          `+blibli+`
          `+bukalapak+`
          `+lazada+`
          `+shopee+`
          `+tokopedia+`
          `+lainnya+`
          `+lainnya2+`
        </div>
      `);
    },
  });
}