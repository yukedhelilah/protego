$(document).ready(function() {
    $('.mobile-menu').css('color','#222');
    $('#home-menu').css('color','#9cc15c');
  	get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Product/get_data/_',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $.each(response.data, function( i, val ) {
      	var img = '';
      	if (val.img1 == '') {
      		img = `https://protegoindonesia.com/server/files/no-image.png`;
      	} else {
      		img = `https://protegoindonesia.com/server/files/product/`+val.img1;
      	}
      	$('#content').append(`
      		<div class="col-md-2 col-ts-6">
                <div class="product-inner tooltip-top">
                    <div class="product-thumb">
                        <a class="thumb-link" href="products_details?search=`+btoa(val.id+`|`+val.category+`|`+val.sub_category+`|`+val.sub_categoryID+`|`+val.product+`|`+val.categoryID)+`" tabindex="0">
                            <img class="img-responsive" src="`+img+`" alt="Sweeper Funnel" width="100%" height="350">
                        </a>
                    </div>
                    <div class="product-info" style="margin-top:15px;text-align:center">
                        <h3 class="product-name product_title">
                            <a href="products_details?search=`+btoa(val.id+`|`+val.category+`|`+val.sub_category+`|`+val.sub_categoryID+`|`+val.product+`|`+val.categoryID)+`" tabindex="0">`+val.product+`</a>
                        </h3>
                        <span class="price">
                        	<span class="furgan-Price-amount amount">
                        		<span class="furgan-Price-currencySymbol">Rp </span>`+numberFormat(val.price)+`
                    		</span>
                		</span>
                    </div>
                </div>
            </div>
        `);
      });
    },
  });
}