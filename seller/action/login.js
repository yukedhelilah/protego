$('input[name="username"]').focus();

$(document).ready(function() { 
    if (localStorage.getItem(btoa('seller_id')) != null && localStorage.getItem(btoa('seller_name')) != null && localStorage.getItem(btoa('seller_username')) != null && localStorage.getItem(btoa('seller_token')) != null && localStorage.getItem(btoa('seller_expired')) != null) {
        window.location.href = '../pages/home';
    }
    
    $('#password').keypress(function(e) {
        if(e.which == 13) {
            login($("#username").val(),$("#password").val());
        }
    });

    $('#login').click(function() {
        login($("#username").val(),$("#password").val());
    });
});

function login(username, password){  
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'https://protegoindonesia.com/server/Login_seller/login/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
            username    : username,
            password    : password,
        },
        success: function(response) {
            if (response.code == '200') {
                localStorage.setItem(btoa('seller_id'), btoa(response.data.id));
                localStorage.setItem(btoa('seller_name'), btoa(response.data.name));
                localStorage.setItem(btoa('seller_username'), btoa(response.data.username));
                localStorage.setItem(btoa('seller_token'), btoa(response.data.token));
                localStorage.setItem(btoa('seller_expired'), btoa(response.data.expired));
                window.location.href = '../pages/home';
            }else{
            	alert('Your account is not registered');
            }
        },
    });
}

function forgotPassword(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'https://protegoindonesia.com/server/Login_seller/forgot_password/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token           : '1b787b70ed2e0de697d731f14b5da57b',
            username        : $("#username").val(),
        },
        success: function(response) {
            if (response.code == '200') {
                var array   = response.data.email.split('@').join(',').split('.').join(',');
                var arr     = array.split(",");

                var name1   = arr[0].charAt(0);
                var name2   = arr[0].charAt(arr[0].length - 1);
                var sub1    = arr[1].charAt(0);
                var sub2    = arr[1].charAt(arr[1].length - 1);

                var noWA    = response.data.mobile.substr(response.data.mobile.length-4, 4);

                $('#reset_name').html(response.data.name);
                $('#reset_whatsappNo').html(`****-****-`+noWA);
                $('#reset_email').html(name1+`*****`+name2+`@`+sub1+`*****`+sub2+`.`+arr[2]);

                // send_authCode(response.data.mobile,response.auth);

                $('#changePassword').click(function() {
                    var authCode = atob(response.auth);
                    var veriCode = $('#verificationCode').val();

                    if (authCode == veriCode) {
                        changePassword(response.data.id, response.data.username);
                    } else {
                        alert('Your authentication code is not verified.');
                    }
                });


                // $('#send_authCode').click(function() {
                //     send_authCode(response.data.mobile,response.auth);
                // });
                $('#reset_1').css('display','none');
                $('#reset_2').css('display','block');
            }else{
                alert("Your data is not verified");
            }
        },
    });
}

function send_authCode(wa, code){
    var whatsapp = wa.replace(/[-+]/g,'');
    if(whatsapp.slice(0,1) == '0'){
        var phone = '62'+whatsapp.substring(1);
    }else if(whatsapp.slice(0,2) == '62'){
        var phone = whatsapp;
    }

    $.ajax({
        type: 'POST',
        url: 'https://tghapisystem.net/wablash/module/single_message',
        dataType: 'json',
        data: {
            'phone'   : phone,
            'message' : `Your authentication code : `+atob(code),
        },
        beforeSend: function() {
            // $('#loader_reset').css('display','block');
            $('#reset_1, #reset_2').css('display','none');
        },
        success: function(responses) {
            if(responses.status==true){
                $('#reset_1').css('display','none');
                $('#reset_2').css('display','block');
            }else{
                alert(responses.message);
            }
        }
    });   
}

function changePassword(id,username){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'https://protegoindonesia.com/server/Login_seller/reset_password/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token      : '1b787b70ed2e0de697d731f14b5da57b',
            id         : id,
            password   : '123456',
        },
        beforeSend: function() {
            // $('#loader_reset').css('display','block');
            $('#reset_1, #reset_2').css('display','none');
        },
        success: function(responses) {
            if (responses.data == true) {
                login(username, '123456');
            }else{
                alert("Your data is not verified");
            }
        },
    });
}