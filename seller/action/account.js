$(document).ready(function() {
    $('.mobile-menu').css('color','#222');
    $('#account-menu').css('color','#9cc15c');

    get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    url: 'https://protegoindonesia.com/server/Seller/get_details/_',
    dataType: 'JSON',
    data : {
      id : sellerID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(responses) {
      $('#name').val(responses.data.name);
      $('#username').val(responses.data.username);
      $('#email').val(responses.data.email);
      $('#mobile').val(responses.data.mobile);
      $('#mobile2').val(responses.data.mobile2);
      $('#mobile3').val(responses.data.mobile3);
    }
  });
}

function save(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Seller/save/_',
    data: $('#form_master').serialize()+"&id="+sellerID,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.code == 200) {
        window.location.href = '../pages/account';
      } else {
        alert('failed, try again later.');
      }
    },
  });
}