$(document).ready(function() {
    $('.mobile-menu').css('color','#222');
    $('#home-menu').css('color','#9cc15c');
  	get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'https://protegoindonesia.com/server/Product/get_data_for_copy/_',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $.each(response.data, function( i, val ) {
      	var img = '';
      	if (val.img1 == null) {
      		img = `https://protegoindonesia.com/server/files/no-image.png`;
      	} else {
      		img = `https://protegoindonesia.com/server/files/product/`+val.img1;
      	}

        if (val.sellerID != sellerID){
        	$('#content').append(`
        		<div class="col-md-2 col-ts-6">
                  <div class="product-inner tooltip-top">
                      <div class="product-thumb">
                          <a class="thumb-link" href="copy_product_add?search=`+btoa(val.id)+`" tabindex="0">
                              <img class="img-responsive" src="`+img+`" alt="Sweeper Funnel" width="100%" height="350">
                          </a>
                      </div>
                      <div class="product-info" style="margin-top:15px;text-align:center">
                          <h3 class="product-name product_title">
                              <a href="copy_product_add?search=`+btoa(val.id)+`" tabindex="0">`+val.product+`</a>
                          </h3>
                          <span class="price">
                          	<span class="furgan-Price-amount amount">
                          		<span class="furgan-Price-currencySymbol">Rp </span>`+numberFormat(val.price)+`
                      		</span>
                  		</span>
                      </div>
                  </div>
              </div>
          `);
        }
      });
    },
  });
}