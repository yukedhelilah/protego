<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png"/>
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,600,600i,700,700i&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/animate.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/chosen.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/jquery.scrollbar.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/lightbox.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/magnific-popup.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/slick.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/fonts/flaticon.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/megamenu.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/dreaming-attribute.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="../../assets/css/wholecss.css"/>
    <title>Protego | Seller</title>
</head>
<body>
<header id="header" class="header style-02 header-dark header-transparent header-sticky">
    <div class="header-wrap-stick">
        <div class="header-position" style="background-color: #f4f4f4;border-bottom: 1px solid #ebebeb;">
            <div class="header-middle">
                <div class="furgan-menu-wapper"></div>
                <div class="header-middle-inner">
                    <div class="header-search-menu">
                        <div class="block-menu-bar">
                            <a class="menu-bar menu-toggle" href="#">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
                    </div>
                    <div class="header-logo-nav">
                        <div class="header-logo">
                            <a href="home"><img alt="Furgan" src="../assets/images/logo.png" class="logo"></a>
                        </div>
                        <div class="box-header-nav menu-nocenter">
                            <ul id="menu-primary-menu"
                                class="clone-main-menu furgan-clone-mobile-menu furgan-nav main-menu">
                                <li id="menu-item-230"
                                    class="menu-item menu-item-type-post_type menu-item-object-megamenu menu-item-230 parent parent-megamenu item-megamenu menu-item-has-children">
                                    <a class="furgan-menu-item-title" title="Home" href="home">Home</a>
                                </li>
                                <li id="menu-item-228"
                                    class="menu-item menu-item-type-post_type menu-item-object-megamenu menu-item-228 parent parent-megamenu item-megamenu menu-item-has-children">
                                    <a class="furgan-menu-item-title" title="Store" href="store">Store</a>
                                </li>
                                <li id="menu-item-229"
                                    class="menu-item menu-item-type-post_type menu-item-object-megamenu menu-item-229 parent parent-megamenu item-megamenu menu-item-has-children">
                                    <a class="furgan-menu-item-title" title="Preorder" href="preorder">Preorder</a>
                                </li>
                                <li id="menu-item-229"
                                    class="menu-item menu-item-type-post_type menu-item-object-megamenu menu-item-229 parent parent-megamenu item-megamenu menu-item-has-children">
                                    <a class="furgan-menu-item-title" title="Account" href="account">Account</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="header-control">
                        <div class="header-control-inner">
                            <div class="meta-dreaming">
                                <a onclick="logout()" style="color: black;cursor: pointer;">
                                    Logout  <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-mobile">
        <div class="header-mobile-left">
        </div>
        <div class="header-mobile-mid">
            <div class="header-logo">
                <a href="home"><img alt="Furgan" src="../assets/images/logo.png" class="logo"></a>
            </div>
        </div>
        <div class="header-mobile-right">
            <div class="header-control-inner">
                <div class="meta-dreaming">
                    <div class="menu-item block-user block-dreaming">
                        <a class="block-link" onclick="logout()">
                            <i class="fa fa-sign-out"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>