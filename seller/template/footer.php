<div class="footer-device-mobile">
    <div class="wapper">
        <div class="footer-device-mobile-item device-home">
            <a href="home" id="home-menu" class="mobile-menu">
				<span class="icon">
					<i class="fa fa-home" aria-hidden="true"></i>
				</span>
                Home
            </a>
        </div>
        <div class="footer-device-mobile-item device-home device-store">
            <a href="store" id="store-menu" class="mobile-menu">
                <span class="icon">
                    <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                </span>
                <span class="text">Store</span>
            </a>
        </div>
        <div class="footer-device-mobile-item device-home device-preorder">
            <a href="preorder" id="preorder-menu" class="mobile-menu">
                <span class="icon">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                </span>
                <span class="text">Preorder</span>
            </a>
        </div>
        <div class="footer-device-mobile-item device-home device-account">
            <a href="account" id="account-menu" class="mobile-menu">
				<span class="icon">
					<i class="fa fa-user" aria-hidden="true"></i>
				</span>
                Account
            </a>
        </div>
    </div>
</div>
<a href="#" class="backtotop active">
    <i class="fa fa-angle-double-up"></i>
</a>
<script src="../assets/js/jquery-1.12.4.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/chosen.min.js"></script>
<script src="../assets/js/countdown.min.js"></script>
<script src="../assets/js/jquery.scrollbar.min.js"></script>
<script src="../assets/js/lightbox.min.js"></script>
<script src="../assets/js/magnific-popup.min.js"></script>
<script src="../assets/js/slick.js"></script>
<script src="../assets/js/jquery.zoom.min.js"></script>
<script src="../assets/js/threesixty.min.js"></script>
<script src="../assets/js/jquery-ui.min.js"></script>
<script src="../assets/js/mobilemenu.js"></script>
<script src="../assets/js/functions.js"></script>
<script src="../action/wholejs.js"></script>
<script src="../function.js"></script>
</body>
</html>