<?php include "../template/header.php"; ?>

    <div class="container" style="padding-top: 100px">
    <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
        <h1 class="page-title">PREORDER</h1>
        <ul class="trail-items breadcrumb nav-preorder" style="text-align: left;float: left;">
            <li class="trail-item trail-begin"><a href="home"><span>HOME</span></a></li>
            <li class="trail-item trail-end active"><span>PREORDER</span></li>
        </ul>
        <ul class="trail-items breadcrumb nav-preorder" style="text-align: right;float: right;">
            <li class="trail-item trail-begin"><a href="new_preorder" class="btn-black"><i class="fa fa-plus"></i> PreOrder</a></li>
        </ul>
    </div>
    <div class="furgan-categories style-02">
        <div class="row" style="padding-bottom: 15px;width: 100%;" id="content">
        </div>
    </div>
    <div class="">
        <hr>
        <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered table-hover" style="font-size:12px">
                <thead>
                    <tr>
                        <th>PRODUCT</th>
                        <th style="text-align:center">QTY</th>
                        <th style="text-align:center">TOTAL</th>
                        <th style="text-align:center">TIMELIMIT</th>
                        <th style="text-align:center">ADDRESS</th>
                        <th style="text-align:center">STATUS</th>
                        <th style="text-align:center">ACTION</th>
                    </tr>
                </thead>
                <tbody id="tb_preorder"></tbody>
            </table>
        </div>
    </div>



<?php include "../template/footer.php"; ?>
<script src="../action/preorder.js"></script>