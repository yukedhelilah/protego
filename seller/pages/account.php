<?php include "../template/header.php"; ?>

<div class="container" style="padding-top: 100px">
    <h1 class="page-title">ACCOUNT</h1>
    <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs" style="text-align: center;">
        <ul class="trail-items breadcrumb">
            <li class="trail-item trail-begin"><a href="home"><span>HOME</span></a></li>
            <li class="trail-item trail-end active"><span>ACCOUNT</span>
            </li>
        </ul>
    </div>
    <form autocomplete="off" id="form_master">
        <input type="hidden" name="act" id="act" value="edit">
        <div class="row" style="width: 100%;padding-top: 50px;margin: 0">
            <div class="col-ts-12 col-md-6 new-product" style="margin-bottom: 20px;padding-right: 30px;text-align: center;left: 50%;transform: translateX(-50%);">
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="name">Name&nbsp;<span class="required">*</span></label>
                    <input type="text" class="furgan-Input furgan-Input--text input-text" name="name" id="name" value="" style="border-color: #c0ccda !important;border-radius: 0.25rem;">
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="username">Username&nbsp;<span class="required">*</span></label>
                    <input class="furgan-Input furgan-Input--text input-text" type="text" name="username" id="username" style="border-color: #c0ccda !important;border-radius: 0.25rem;">
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="password">Password&nbsp;<span class="required">*</span></label>
                    <input class="furgan-Input furgan-Input--text input-text" type="password" name="password" id="password" style="border-color: #c0ccda !important;border-radius: 0.25rem;">
                    <small style="color: red">*Kosongi jika tidak ingin mengganti password.</small>
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="email">Email&nbsp;<span class="required">*</span></label>
                    <input class="furgan-Input furgan-Input--text input-text" type="email" name="email" id="email" style="border-color: #c0ccda !important;border-radius: 0.25rem;">
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="mobile">Mobile&nbsp;<span class="required">*</span></label>
                    <input class="furgan-Input furgan-Input--text input-text" type="text" name="mobile" id="mobile" style="border-color: #c0ccda !important;border-radius: 0.25rem;">
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="mobile2">Mobile 2&nbsp;<span class="required">*</span></label>
                    <input class="furgan-Input furgan-Input--text input-text" type="text" name="mobile2" id="mobile2" style="border-color: #c0ccda !important;border-radius: 0.25rem;">
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="mobile3">Mobile 3&nbsp;<span class="required">*</span></label>
                    <input class="furgan-Input furgan-Input--text input-text" type="text" name="mobile3" id="mobile3" style="border-color: #c0ccda !important;border-radius: 0.25rem;">
                </p>
                <a href="javascript:" onclick="save()" class="btn-green">SAVE CHANGES</a>
            </div>
        </div>
    </form>
</div>

<?php include "../template/footer.php"; ?>
<script src="../action/account.js"></script>

<style type="text/css">
    #name{
        text-transform: uppercase;
    }

    ::-webkit-input-placeholder { /* WebKit browsers */
    text-transform: none;
    }
    :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
      text-transform: none;
    }
    ::-moz-placeholder { /* Mozilla Firefox 19+ */
      text-transform: none;
    }
    :-ms-input-placeholder { /* Internet Explorer 10+ */
      text-transform: none;
    }
    ::placeholder { /* Recent browsers */
      text-transform: none;
    }

    @media only screen and (max-width: 630px) {
        .row{
            padding-top: 30px !important;
        }
    }
</style>