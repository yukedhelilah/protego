<?php include "../template/header.php"; ?>

<div class="container" style="padding-top: 100px">
    <h1 class="page-title">EDIT PRODUCT</h1>
    <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs" style="text-align: center;">
        <ul class="trail-items breadcrumb">
            <li class="trail-item trail-begin"><a href="home"><span>HOME</span></a></li>
            <li class="trail-item trail-begin"><a href="store"><span>STORE</span></a></li>
            <li class="trail-item trail-end active"><span>EDIT PRODUCT</span>
            </li>
        </ul>
    </div>
    <form autocomplete="off" id="form_master">
        <input type="hidden" name="act" id="act" value="edit">
        <div class="row" style="width: 100%;padding-top: 50px;margin: 0">
            <div class="col-ts-12 col-md-6 new-product" style="margin-bottom: 20px;padding-right: 30px">
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="categoryID">Category&nbsp;<span class="required">*</span></label>
                    <select name="categoryID" id="categoryID" style="border-color: #c0ccda !important;border-radius: 0.25rem;background: none;width: 100%;height: 52px">
                    </select>
                    <input type="hidden" id="category">
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="sub_categoryID">Sub Category&nbsp;<span class="required">*</span></label>
                    <select name="sub_categoryID" id="sub_categoryID" style="border-color: #c0ccda !important;border-radius: 0.25rem;background: none;width: 100%;height: 52px">
                    </select>
                    <input type="hidden" id="sub_category">
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="product">Product&nbsp;<span class="required">*</span></label>
                    <input type="text" class="furgan-Input furgan-Input--text input-text" name="product" id="product" value="" style="border-color: #c0ccda !important;border-radius: 0.25rem;">
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="price">Price&nbsp;<span class="required">*</span></label>
                    <input class="furgan-Input furgan-Input--text input-text" type="number" name="price" id="price" style="border-color: #c0ccda !important;border-radius: 0.25rem;">
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="price">Description&nbsp;<span class="required">*</span></label>
                    <textarea class="furgan-Input furgan-Input--text input-text editor" id="description" name="description" style="border-color: #c0ccda !important;border-radius: 0.25rem;"></textarea>
                </p>
            </div>
            <div class="col-ts-12 col-md-6 new-product" style="margin-bottom: 20px;padding-right: 30px">
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="blibli">Link Product&nbsp;<span class="required">*</span></label>
                    <input class="furgan-Input furgan-Input--text input-text" type="text" name="blibli" id="blibli" style="border-color: #c0ccda !important;border-radius: 0.25rem;" placeholder="Blibli">
                    <input class="furgan-Input furgan-Input--text input-text" type="text" name="bukalapak" id="bukalapak" style="border-color: #c0ccda !important;border-radius: 0.25rem;margin-top: 30px" placeholder="Bukalapak">
                    <input class="furgan-Input furgan-Input--text input-text" type="text" name="lazada" id="lazada" style="border-color: #c0ccda !important;border-radius: 0.25rem;margin-top: 30px" placeholder="Lazada">
                    <input class="furgan-Input furgan-Input--text input-text" type="text" name="shopee" id="shopee" style="border-color: #c0ccda !important;border-radius: 0.25rem;margin-top: 30px" placeholder="Shopee">
                    <input class="furgan-Input furgan-Input--text input-text" type="text" name="tokopedia" id="tokopedia" style="border-color: #c0ccda !important;border-radius: 0.25rem;margin-top: 30px" placeholder="Tokopedia">
                    <input class="furgan-Input furgan-Input--text input-text" type="text" name="lainnya" id="lainnya" style="border-color: #c0ccda !important;border-radius: 0.25rem;margin-top: 30px" placeholder="Lainnya">
                    <input class="furgan-Input furgan-Input--text input-text" type="text" name="lainnya2" id="lainnya2" style="border-color: #c0ccda !important;border-radius: 0.25rem;margin-top: 30px" placeholder="Lainnya">
                    <div id="divCopy" style="margin-top: 20px;text-align: right;display: none">
                        <input type="hidden" name="isCopy" id="isCopy">
                        <small id="copy"></small>
                    </div>
                </p>
            </div>
            <div class="col-ts-12 new-product" style="text-align: center;margin: 15px 0;padding: 0 15px">
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide" style="margin: 0">
                    <label for="Category" style="margin-bottom: 0;margin-top: 5px">Images</label>
                    <div class="row" style="padding: 0 !important">
                        <div class="img-product" style="width:20%;padding:2%">
                            <input type="file" name="file_img1" id="file_img1" style="border-radius: 0.25rem;width: 100%;margin-bottom: 10px">
                            <input type="hidden" name="img1" id="img1">
                            <img src="" id="img_product1" width="100%">
                            <div id="txt_img1"></div>
                        </div>
                        <div class="img-product" style="width:20%;padding:2%">
                            <input type="file" name="file_img2" id="file_img2" style="border-radius: 0.25rem;width: 100%;margin-bottom: 10px">
                            <input type="hidden" name="img2" id="img2">
                            <img src="" id="img_product2" width="100%">
                            <div id="txt_img2"></div>
                        </div>
                        <div class="img-product" style="width:20%;padding:2%">
                            <input type="file" name="file_img3" id="file_img3" style="border-radius: 0.25rem;width: 100%;margin-bottom: 10px">
                            <input type="hidden" name="img3" id="img3">
                            <img src="" id="img_product3" width="100%">
                            <div id="txt_img3"></div>
                        </div>
                        <div class="img-product" style="width:20%;padding:2%">
                            <input type="file" name="file_img4" id="file_img4" style="border-radius: 0.25rem;width: 100%;margin-bottom: 10px">
                            <input type="hidden" name="img4" id="img4">
                            <img src="" id="img_product4" width="100%">
                            <div id="txt_img4"></div>
                        </div>
                        <div class="img-product" style="width:20%;padding:2%">
                            <input type="file" name="file_img5" id="file_img5" style="border-radius: 0.25rem;width: 100%;margin-bottom: 10px">
                            <input type="hidden" name="img5" id="img5">
                            <img src="" id="img_product5" width="100%">
                            <div id="txt_img5"></div>
                        </div>
                    </div>
                </p>
                <a href="javascript:" onclick="save()" class="btn-green">SAVE</a>
            </div>
        </div>
    </form>
</div>
<?php include "../template/footer.php"; ?>
<script src="//cdn.ckeditor.com/4.13.1/basic/ckeditor.js"></script>
<script src="../action/edit_product.js"></script>

<style type="text/css">
    #product{
        text-transform: uppercase;
    }

    ::-webkit-input-placeholder { /* WebKit browsers */
    text-transform: none;
    }
    :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
      text-transform: none;
    }
    ::-moz-placeholder { /* Mozilla Firefox 19+ */
      text-transform: none;
    }
    :-ms-input-placeholder { /* Internet Explorer 10+ */
      text-transform: none;
    }
    ::placeholder { /* Recent browsers */
      text-transform: none;
    }

    @media only screen and (max-width: 1024px) {
        .row{
            padding-top: 30px !important;
        }

        .img-product{
            width: 50% !important;
        }
    }
</style>