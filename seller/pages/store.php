<?php include "../template/header.php"; ?>

<div class="container" style="padding-top: 100px">
    <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
        <h1 class="page-title">STORE</h1>
        <ul class="trail-items breadcrumb nav-store" style="text-align: left;float: left;">
            <li class="trail-item trail-begin"><a href="home"><span>HOME</span></a></li>
            <li class="trail-item trail-end active"><span>STORE</span></li>
        </ul>
        <ul class="" style="text-align: right;float: right;list-style: none;flex-wrap: wrap;">
            <li style="display: inline-block !important;"><a href="new_product" class="btn-black"><i class="fa fa-plus"></i> Add</a></li>
            <li style="display: inline-block !important;"><a href="copy_product" class="btn-black"><i class="fa fa-copy"></i> Copy</a></li>
        </ul>
    </div>
    <div class="furgan-categories style-02">
        <div class="row" style="padding-bottom: 15px;width: 100%;" id="content">
        </div>
    </div>
</div>

<?php include "../template/footer.php"; ?>
<script src="../action/store.js"></script>

<style type="text/css">
    @media only screen and (max-width: 630px){
        .btn-black {
            min-width: 65px;
        }
    }
</style>