<?php include "../template/header.php"; ?>

<div class="container" style="padding-top: 100px">
    <h1 class="page-title">NEW PREORDER</h1>
    <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs" style="text-align: center;">
        <ul class="trail-items breadcrumb">
            <li class="trail-item trail-begin"><a href="home"><span>HOME</span></a></li>
            <li class="trail-item trail-begin"><a href="preorder"><span>PREORDER</span></a></li>
            <li class="trail-item trail-end active"><span>NEW PREORDER</span>
            </li>
    </div>
        </ul>
    </div>
    <form autocomplete="off" id="form_master">
        <input type="hidden" name="act" id="act" value="add">
        <input type="hidden" name="id" id="id">
        <div class="row" style="width: 100%;padding-top: 50px;margin: 0">
            <div class="col-ts-12 col-md-6 new-product" style="margin-bottom: 20px;padding-right: 30px;text-align: center;left: 50%;transform: translateX(-50%);">
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="categoryID">Category&nbsp;<span class="required">*</span></label>
                    <select name="categoryID" id="categoryID" style="border-color: #c0ccda !important;border-radius: 0.25rem;background: none;width: 100%;height: 52px">
                    </select>
                    <input type="hidden" id="category">
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="sub_categoryID">Sub Category&nbsp;<span class="required">*</span></label>
                    <select name="sub_categoryID" id="sub_categoryID" style="border-color: #c0ccda !important;border-radius: 0.25rem;background: none;width: 100%;height: 52px">
                    </select>
                    <input type="hidden" id="sub_category">
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="productID">Product&nbsp;<span class="required">*</span></label>
                    <select name="productID" id="productID" style="border-color: #c0ccda !important;border-radius: 0.25rem;background: none;width: 100%;height: 52px">
                    </select>
                    <input type="hidden" id="product">
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="qty">Quantity&nbsp;<span class="required">*</span></label>
                    <input class="furgan-Input furgan-Input--text input-text" type="number" name="qty" id="qty" style="border-color: #c0ccda !important;border-radius: 0.25rem;">
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="type">Type&nbsp;<span class="required">*</span></label>
                    <input class="furgan-Input furgan-Input--text input-text" type="text" name="type" id="type" style="border-color: #c0ccda !important;border-radius: 0.25rem;">
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="price">Remarks&nbsp;<span class="required">*</span></label>
                    <textarea class="furgan-Input furgan-Input--text input-text editor" id="remarks" name="remarks" style="border-color: #c0ccda !important;border-radius: 0.25rem;"></textarea>
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="deadline">Date&nbsp;<span class="required">*</span></label>
                    <input class="furgan-Input furgan-Input--text input-text" type="date" name="deadline" id="deadline" style="border-color: #c0ccda !important;border-radius: 0.25rem;" data-date="" data-date-format="DD MMMM YYYY">
                </p>
                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                    <label for="status">Status&nbsp;<span class="required">*</span></label>
                    <select name="status" id="status" style="border-color: #c0ccda !important;border-radius: 0.25rem;background: none;width: 100%;height: 52px">
                        <option value="0">Waiting List</option>
                        <option value="1">Done</option>
                    </select>
                </p>
                <a href="javascript:" onclick="save()" class="btn-green">SAVE</a>
            </div>
        </div>
    </form>
</div>
<?php include "../template/footer.php"; ?>
<script src="//cdn.ckeditor.com/4.13.1/basic/ckeditor.js"></script>
<script src="../action/new_preorder.js"></script>

<style type="text/css">
    #product{
        text-transform: uppercase;
    }

    ::-webkit-input-placeholder { /* WebKit browsers */
    text-transform: none;
    }
    :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
      text-transform: none;
    }
    ::-moz-placeholder { /* Mozilla Firefox 19+ */
      text-transform: none;
    }
    :-ms-input-placeholder { /* Internet Explorer 10+ */
      text-transform: none;
    }
    ::placeholder { /* Recent browsers */
      text-transform: none;
    }

    @media only screen and (max-width: 1024px) {
        .row{
            padding-top: 30px !important;
        }

        .img-product{
            width: 50% !important;
        }
    }
</style>