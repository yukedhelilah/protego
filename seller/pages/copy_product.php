
<?php include "../template/header.php"; ?>

<div class="container" style="padding-top: 100px">
    <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
        <h1 class="page-title">COPY PRODUCT</h1>
        <ul class="trail-items breadcrumb nav-store" style="text-align: left;float: left;">
            <li class="trail-item trail-begin"><a href="home"><span>HOME</span></a></li>
            <li class="trail-item"><a href="store"><span>STORE</span></a></li>
            <li class="trail-item trail-end active"><span>COPY PRODUCT</span></li>
        </ul>
    </div>
    <div class="furgan-categories style-02">
        <div class="row" style="padding-bottom: 15px;width: 100%;" id="content">
        </div>
    </div>
</div>

<?php include "../template/footer.php"; ?>
<script src="../action/copy_product.js"></script>