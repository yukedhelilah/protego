<?php include "../template/header.php"; ?>

<div class="single-thumb-vertical shop-page no-sidebar" style="margin: 0;padding: 0">
    <div class="container" style="padding-top: 115px;">
        <!-- <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs" style="margin-top: 0;margin-bottom: 10px;padding: 0 10px">
            <ul class="trail-items breadcrumb">
                <li class="trail-item trail-begin"><a href="index"><span>HOME</span></a></li>
                <li class="trail-item"><a href="category"><span>CATEGORY</span></a></li>
                <li class="trail-item"><a id="link-product"><span id="nav-sub-category"></span></a></li>
            </ul>
        </div> -->
        <div class="row">
            <!-- <div class="main-content col-md-12" style="padding: 0 10px"> -->
                <div class="col-lg-5 col-ts-12 div-mobile" style="padding-right: 4%">
                    <!-- <div class="flex-viewport">
                        <figure class="furgan-product-gallery__wrapper"> -->
                            <div class="div-img" id="div-img1"><img id="img1" src="" alt="img" style="width: 100%"></div>
                            <div class="div-img" id="div-img2" style="display: none"><img id="img2" src="" alt="img" style="width: 100%"></div>
                            <div class="div-img" id="div-img3" style="display: none"><img id="img3" src="" alt="img" style="width: 100%"></div>
                            <div class="div-img" id="div-img4" style="display: none"><img id="img4" src="" alt="img" style="width: 100%"></div>
                            <div class="div-img" id="div-img5" style="display: none"><img id="img5" src="" alt="img" style="width: 100%"></div>
                        <!-- </figure>
                    </div> -->
                    <div class="row" style="margin: 30px 0">
                        <div class="icon-img" id="btn-img1" style="box-shadow: 2px 2px 7px #484848;"><img id="icon-img1" style="width: 100%" src=""></div>
                        <div class="icon-img" id="btn-img2"><img id="icon-img2" style="width: 100%" src=""></div>
                        <div class="icon-img" id="btn-img3"><img id="icon-img3" style="width: 100%" src=""></div>
                        <div class="icon-img" id="btn-img4"><img id="icon-img4" style="width: 100%" src=""></div>
                        <div class="icon-img" id="btn-img5"><img id="icon-img5" style="width: 100%" src=""></div>
                    </div>
                </div>
                <div class="col-lg-7 col-ts-12">
                    <div class="summary entry-summary">
                        <h1 class="product_title entry-title" id="product"></h1>
                        <p class="price">
                            <ins><span class="furgan-Price-amount amount" id="price"></span></ins>
                        </p>
                        <div class="furgan-product-details__short-description" id="description">
                        </div>
                        <div class="product_meta">
                            <div>
                                <div class="furgan-iconbox style-01">
                                    <div class="iconbox-inner">
                                        <div class="content" id="contact">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="furgan-notices-wrapper"></div>
                <div id="product-27" class="post-27 product type-product status-publish has-post-thumbnail product_cat-table product_cat-new-arrivals product_cat-lamp product_tag-table product_tag-sock first instock shipping-taxable purchasable product-type-variable has-default-attributes">
                    <div class="main-contain-summary">
                        <div class="contain-left has-gallery" id="content">
                            <div class="single-left">
                                <div class="furgan-product-gallery furgan-product-gallery--with-images furgan-product-gallery--columns-4 images">
                                    <div class="flex-viewport">
                                        <figure class="furgan-product-gallery__wrapper">
                                            <div class="furgan-product-gallery__image"><img id="img1" src="" alt="img" style="width: 100%">
                                            </div>
                                            <div class="furgan-product-gallery__image"><img id="img2" src="" alt="img" style="width: 100%">
                                            </div>
                                            <div class="furgan-product-gallery__image"><img id="img3" src="" alt="img" style="width: 100%">
                                            </div>
                                            <div class="furgan-product-gallery__image"><img id="img4" src="" alt="img" style="width: 100%">
                                            </div>
                                            <div class="furgan-product-gallery__image"><img id="img5" src="" alt="img" style="width: 100%">
                                            </div>
                                        </figure>
                                    </div>
                                    <ol class="flex-control-nav flex-control-thumbs">
                                        <li><img id="icon-img1" src="" alt="img"></li>
                                        <li><img id="icon-img2" src="" alt="img"></li>
                                        <li><img id="icon-img3" src="" alt="img"></li>
                                        <li><img id="icon-img4" src="" alt="img"></li>
                                        <li><img id="icon-img5" src="" alt="img"></li>
                                    </ol>
                                </div>
                            </div>
                            <div class="summary entry-summary">
                                <h1 class="product_title entry-title" id="product"></h1>
                                <p class="price">
                                    <ins><span class="furgan-Price-amount amount" id="price"></span></ins>
                                </p>
                                <div class="furgan-product-details__short-description" id="description">
                                </div>
                                <div class="product_meta">
                                    <div>
                                        <div class="furgan-iconbox style-01">
                                            <div class="iconbox-inner">
                                                <div class="content" id="contact">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            <!-- </div> -->
        </div>
    </div>
</div>

<?php include "../template/footer.php"; ?>
<script src="../action/products_detail.js"></script>

<style type="text/css">
    .div-img{
        box-shadow: 5px 5px 10px #d4d4d4;
    }

    .icon-img{
        width: 18%;
        margin: 1%;
        padding: 0.5%;
        border: 1px solid #e0e0e0;
        cursor: pointer;
    }

    @media only screen and (max-width: 630px) {
        .div-mobile{
            padding-right: 5px !important; 
        }
    }
</style>

<!-- box-shadow: 2px 2px 7px #484848; -->