<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png"/>
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,600,600i,700,700i&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css"/>
    <title>Protego</title>
</head>
<body style="margin: 0;background-color: #f6f6f6">

<div class="container">
    <div class="row">
        <div class="main-content col-ts-11 col-md-6 col-md-offset-3" style="left: 50%;top: 50%;transform: translate(-50%, -50%);position: absolute;">
            <div class="page-main-content">
                <div class="furgan">
                    <div class="furgan-notices-wrapper"></div>
                    <div class="u-columns col2-set" id="customer_login">
                        <div class="u-column1 col-1" style="width: 100%; padding: 30px !important;margin: 0;text-align: left;" id="reset_1">
                            <h3>FORGOT PASSWORD</h3>
                            <h4>Please enter your Username and we'll send you instruction to reset your password.</h4>
                            <form class="furgan-form furgan-form-login login" method="post" autocomplete="off" style="margin-top: 50px">
                                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                                    <label for="username">Username&nbsp;<span class="required">*</span></label>
                                    <input type="text" class="furgan-Input furgan-Input--text input-text" name="username" id="username" value="" style="border-color: #c0ccda !important;border-radius: 0.25rem;">
                                </p>
                                <p class="form-row" style="margin-bottom: 0">
                                    <input type="hidden" id="furgan-login-nonce" name="furgan-login-nonce" value="832993cb93"><input type="hidden" name="_wp_http_referer" value="/furgan/my-account/">
                                    <a class="furgan-Button button" onclick="forgotPassword()" style="color: white;float: none">Submit</a>
                                </p>
                            </form>
                        </div>
                        <div class="u-column1 col-1" style="width: 100%; padding: 30px !important;margin: 0;text-align: left;display: none" id="reset_2">
                            <h4>
                                Hi, <span style="font-weight: bold;" id="reset_name"></span><br>
                                We've sent a whatsapp massage to <span style="font-weight: bold;" id="reset_whatsappNo"></span><br>
                                and email to <span style="font-weight: bold;" id="reset_email"></span><br>
                                with your authentication code. 
                            </h4>
                            <form class="furgan-form furgan-form-login login" method="post" autocomplete="off" style="margin-top: 50px">
                                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                                    <label for="verificationCode">Authentication Code&nbsp;<span class="required">*</span></label>
                                    <input type="text" class="furgan-Input furgan-Input--text input-text" name="verificationCode" id="verificationCode" value="" style="border-color: #c0ccda !important;border-radius: 0.25rem;">
                                </p>
                                <p class="form-row" style="margin-bottom: 0">
                                    <input type="hidden" id="furgan-login-nonce" name="furgan-login-nonce" value="832993cb93"><input type="hidden" name="_wp_http_referer" value="/furgan/my-account/">
                                    <a class="furgan-Button button" id="changePassword" style="color: white;float: none">Verify Code</a>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="../assets/js/jquery-1.12.4.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../action/login.js"></script>
</body>
</html>