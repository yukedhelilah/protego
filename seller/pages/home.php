<?php include "../template/header.php"; ?>

<div class="section-001" style="padding: 0">
    <div class="container" style="padding-top: 100px">
        <div class="furgan-heading style-01" style="padding-top: 15px;padding-bottom: 15px">
            <div class="heading-inner">
                <h3 class="title">ALL PRODUCT</h3>
            </div>
        </div>
        <div class="furgan-products style-02">
            <div class="row" id="content">
            </div>
        </div>
    </div>
</div>

<?php include "../template/footer.php"; ?>
<script src="../action/home.js"></script>