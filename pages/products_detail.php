<?php include "../template/header.php"; ?>

<div class="single-thumb-vertical shop-page no-sidebar" style="margin: 0;padding: 0">
    <div class="container" style="padding-top: 115px;">
        <div class="row">
            <div class="col-lg-5 col-ts-12 div-mobile" style="padding-right: 4%">
                <div class="div-img" id="div-img1"><img id="img1" src="" alt="img" style="width: 100%"></div>
                <div class="div-img" id="div-img2" style="display: none"><img id="img2" src="" alt="img" style="width: 100%"></div>
                <div class="div-img" id="div-img3" style="display: none"><img id="img3" src="" alt="img" style="width: 100%"></div>
                <div class="div-img" id="div-img4" style="display: none"><img id="img4" src="" alt="img" style="width: 100%"></div>
                <div class="div-img" id="div-img5" style="display: none"><img id="img5" src="" alt="img" style="width: 100%"></div>
                <div class="row" style="margin: 30px 0">
                    <div class="icon-img" id="btn-img1" style="box-shadow: 2px 2px 7px #484848;"><img id="icon-img1" style="width: 100%" src=""></div>
                    <div class="icon-img" id="btn-img2"><img id="icon-img2" style="width: 100%" src=""></div>
                    <div class="icon-img" id="btn-img3"><img id="icon-img3" style="width: 100%" src=""></div>
                    <div class="icon-img" id="btn-img4"><img id="icon-img4" style="width: 100%" src=""></div>
                    <div class="icon-img" id="btn-img5"><img id="icon-img5" style="width: 100%" src=""></div>
                </div>
                <div id="div-qty" style="margin-top: 45px;padding-bottom: 45px">
                </div>   
                <input type="hidden" id="qty" name="qty" value="1">           
            </div>
            <div class="col-lg-7 col-ts-12">
                <div class="summary entry-summary">
                    <h1 class="product_title entry-title" id="product"></h1>
                    <p class="price">
                        <ins><span class="furgan-Price-amount amount" id="price"></span></ins>
                    </p>
                    <div class="furgan-product-details__short-description" id="description">
                    </div>
                    <div class="product_meta">
                        <div>
                            <div class="furgan-iconbox style-01">
                                <div class="iconbox-inner">
                                    <div class="content" id="contact">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include "../template/footer.php"; ?>
<script src="../action/products_detail.js"></script>

<style type="text/css">
    .div-img{
        box-shadow: 5px 5px 10px #d4d4d4;
    }

    .icon-img{
        width: 18%;
        margin: 1%;
        padding: 0.5%;
        border: 1px solid #e0e0e0;
        cursor: pointer;
    }

    @media only screen and (max-width: 630px) {
        .div-mobile{
            padding-right: 5px !important; 
        }
    }
</style>

<!-- box-shadow: 2px 2px 7px #484848; -->