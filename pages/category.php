<?php include "../template/header.php"; ?>

<div class="section-043" style="padding: 0">
    <div class="container" style="padding-top: 100px;text-align: center;">
        <!-- <h1 class="page-title">CATEGORY</h1>
        <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
            <ul class="trail-items breadcrumb">
                <li class="trail-item trail-begin"><a href="index"><span>HOME</span></a></li>
                <li class="trail-item trail-end active"><span>CATEGORY</span>
                </li>
            </ul>
        </div> -->
        <div class="furgan-categories  style-02">
            <div class="row" style="padding-bottom: 15px;padding-top: 0px;margin-bottom: 20px" id="content">
            </div>
            <div class="row" id="content-product" style="border: 1px solid #f1f1f1;margin: 0;padding: 20px 10px;border-radius: 12px;">
                <div style="width: 100%">
                    <div class="furgan-heading style-01" style="padding-bottom: 15px">
                        <div class="heading-inner">
                            <h3 class="title-mobile" style="font-size: 28px;margin-top: 10px">NEW ARRIVAL</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include "../template/footer.php"; ?>
<script src="../action/category.js"></script>

<style type="text/css">
@media only screen and (max-width: 630px) {
    .title-mobile{
        font-size: 20px !important;
    }

    .font-mobile{
        font-size: 13px;
    }
}   
</style>