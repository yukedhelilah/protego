<?php include "../template/header.php"; ?>

<div class="section-043" style="padding: 0">
    <div class="container" style="padding-top: 90px;text-align: center;">
        <h1 class="page-title" id="title"></h1>
        <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
            <ul class="trail-items breadcrumb">
                <li class="trail-item trail-begin"><a href="index"><span>HOME</span></a></li>
                <li class="trail-item"><a href="category"><span>CATEGORY</span></a></li>
                <li class="trail-item trail-end active"><span id="nav-category"></span></li>
            </ul>
        </div>
        <div class="furgan-categories  style-02">
            <div class="row" style="padding-bottom: 15px" id="content">
            </div>
        </div>
    </div>
</div>

<?php include "../template/footer.php"; ?>
<script src="../action/sub_category.js"></script>