<?php include "../template/header.php"; ?>

    <div class="container" style="padding-top: 115px;">
        <div class="furgan-heading style-01" style="padding-bottom: 15px">
            <div class="heading-inner">
                <h3 class="title">NEW ARRIVAL</h3>
            </div>
        </div>
        <div class="furgan-products style-02">
            <div class="row" id="content">
            </div>
        </div>
    </div>

<?php include "../template/footer.php"; ?>
<script src="../action/home.js"></script>

<style type="text/css">
    @media only screen and (max-width: 1024px) {
        .section-002{
            padding-top: 0 !important;
        }
    }
</style>