<?php include "../template/header.php"; ?>

<div class="section-043" style="padding: 0">
    <div class="container" style="padding-top: 100px;text-align: center;">
        <h1 class="page-title" id="title">CART</h1>
        <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs" style="margin-bottom: 15px">
            <ul class="trail-items breadcrumb">
                <li class="trail-item trail-begin"><a href="index"><span>HOME</span></a></li>
                <li class="trail-item trail-end active"><span>CART</span></li>
            </ul>
        </div>
        <div class="furgan-products style-04">
            <div class="row mobile-none tab-none">
	            <div class="col-ts-12 col-sm-12 col-md-12" style="margin:15px 0 0 0;border-top: 1px solid #e0e0e0;padding: 15px 10px 0 10px">
		            <div class="row">
		            	<div class="col-ts-5 col-sm-5 col-md-2">
	                        <div style="width: 30%;float: left;">
	                        </div>
	                        <div style="width: 70%;float: right;">
	                        	PRODUCT
	                        </div>
		            	</div>
		            	<div class="col-ts-7 col-sm-7 col-md-10">
		            		<div class="row" style="top: 50%;transform: translateY(-50%);position: relative;">
		            			<div class="col-ts-10 col-sm-10 col-md-11" style="padding-left:15px">
		            				<div class="row">
				            			<div class="col-ts-12 col-sm-12 col-md-5">
					                        <h3 class="product-name product_title align-left-mobile" style="text-align: left;">
					                            <a href="#" tabindex="0">
					                           </a>
					                        </h3>
				            			</div>
				            			<div class="col-ts-12 col-sm-12 col-md-2 align-left-mobile mobile-none">
				            				AMOUNT
				            			</div>
				            			<div class="col-ts-12 col-sm-12 col-md-2 align-left-mobile">
				            				TOTAL AMOUNT
				            			</div>
				            			<div class="col-ts-12 col-sm-12 col-md-3">
				            				QUANTITY
				            			</div>
		            				</div>
		            			</div>
		            			<div class="col-ts-2 col-sm-10 col-md-1">
		            				<div class="row" style="top: 50%;transform: translateY(-50%);position: relative;">
				            			<div class="col-ts-12 col-md-12">
				            				ACTION
				            			</div>
		            				</div>
		            			</div>
		            		</div>
		            	</div>
		            </div>
	            </div>
            </div>
            <div class="row" id="content">
            </div>
            <div class="row">
	            <div class="col-ts-12 col-sm-12 col-md-12" style="margin:15px 0;border-top: 1px dashed #e0e0e0;border-bottom: 1px solid #e0e0e0;padding: 15px 10px">
		            <div class="row">
		            	<div class="col-ts-5 col-sm-5 col-md-2">
		            		<div class="row" style="top: 50%;transform: translateY(-50%);position: relative;">
		                        <div class="col-ts-2 col-sm-2 col-md-4">
			            			<input class="input-checkbox" type="checkbox" name="checkAll" id="checkAll" style="top: 50%;transform: translateY(-50%);position: relative;">
		                        </div>
		                        <div class="col-ts-10 col-sm-10 col-md-8">
			                        <h3 class="product-name product_title align-left-mobile" style="top: 53%;transform: translateY(-50%);position: relative;">
			                        	TOTAL
			                        </h3>
		                        </div>
		            		</div>
		            	</div>
		            	<div class="col-ts-7 col-sm-7 col-md-10">
		            		<div class="row" style="top: 50%;transform: translateY(-50%);position: relative;">
		            			<div class="col-ts-12 col-sm-12 col-md-12" style="padding-left:15px">
		            				<div class="row">
				            			<div class="col-ts-12 col-sm-12 col-md-5">
					                        <h3 class="product-name product_title align-left-mobile" style="text-align: left;">
					                            <a href="#" tabindex="0">
					                           </a>
					                        </h3>
				            			</div>
				            			<div class="col-ts-6 col-sm-6 col-md-5" style="text-align: left;">
					                        <span class="price" style="top: 50%;transform: translateY(-50%);position: relative;">
					                            <span class="furgan-Price-amount amount" id="grandTotal"></span>
					                        </span>
				            			</div>
				            			<div class="col-ts-6 col-sm-6 col-md-2">
				            				<button class="btn-green2" style="width: 100%;height: auto;" onclick="checkout()">Checkout</button>
				            			</div>
		            				</div>
		            			</div>
		            		</div>
		            	</div>
		            </div>
	            </div>
            </div>
            <input type="hidden" id="address">
            <input type="hidden" id="province">
            <input type="hidden" id="city">
            <input type="hidden" id="subDistrict">
            <input type="hidden" id="zipCode">
        </div>
    </div>
</div>

<?php include "../template/footer.php"; ?>
<script src="../action/cart.js"></script>