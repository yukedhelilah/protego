<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png"/>
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,600,600i,700,700i&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css"/>
    <title>Protego</title>
</head>
<body style="margin: 0;background-color: #f6f6f6">

<div class="container">
    <div class="row">
        <div class="main-content col-ts-11 col-md-6 col-md-offset-3" style="left: 50%;transform: translateX(-50%);position: absolute;">
            <div class="page-main-content">
                <div class="furgan">
                    <div class="furgan-notices-wrapper"></div>
                    <div class="u-columns col2-set" id="customer_login">
                        <div class="u-column1 col-1" style="width: 100%; padding: 30px !important;margin: 0;text-align: center;">
                            <!-- <h2>Login</h2> -->
                            <img src="../assets/images/logo.png" style="margin-bottom: 40px">
                            <form class="furgan-form furgan-form-login login" method="post" autocomplete="off" id="form_signup">
                                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                                    <label for="name">Name&nbsp;<span class="required">*</span></label>
                                    <input type="text" class="furgan-Input furgan-Input--text input-text" name="name" id="name" value="" style="border-color: #c0ccda !important;border-radius: 0.25rem;" onKeyUp="requiredField(this.value,'name')" onblur="requiredField(this.value,'name')">
                                    <input type="hidden" name="isname" id="isname" value="0">
                                </p>
                                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                                    <label for="email">Email&nbsp;<span class="required">*</span></label>
                                    <input type="text" class="furgan-Input furgan-Input--text input-text" name="email" id="email" value="" style="border-color: #c0ccda !important;border-radius: 0.25rem;" onKeyUp="requiredField(this.value,'email')" onblur="requiredField(this.value,'email')">
                                    <input type="hidden" name="isemail" id="isemail" value="0">
                                </p>
                                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                                    <label for="mobile">Mobile&nbsp;<span class="required">*</span></label>
                                    <input type="number" class="furgan-Input furgan-Input--text input-text" name="mobile" id="mobile" value="" style="border-color: #c0ccda !important;border-radius: 0.25rem;" onKeyUp="requiredField(this.value,'mobile')" onblur="requiredField(this.value,'mobile')">
                                    <input type="hidden" name="ismobile" id="ismobile" value="0">
                                </p>
                                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                                    <label for="username">Username&nbsp;<span class="required">*</span></label>
                                    <input type="text" class="furgan-Input furgan-Input--text input-text" name="username" id="username" value="" style="border-color: #c0ccda !important;border-radius: 0.25rem;" onKeyUp="requiredField(this.value,'username')" onblur="requiredField(this.value,'username')">
                                    <input type="hidden" name="isusername" id="isusername" value="0">
                                </p>
                                <p class="furgan-form-row furgan-form-row--wide form-row form-row-wide">
                                    <label for="password">Password&nbsp;<span class="required">*</span></label>
                                    <input class="furgan-Input furgan-Input--text input-text" type="text" name="password" id="password" style="border-color: #c0ccda !important;border-radius: 0.25rem;" onKeyUp="requiredField(this.value,'password')" onblur="requiredField(this.value,'password')">
                                    <input type="hidden" name="ispassword" id="ispassword" value="0">
                                </p>
                                <p class="form-row" style="margin-bottom: 10px">
                                    <input type="hidden" id="furgan-login-nonce" name="furgan-login-nonce" value="832993cb93"><input type="hidden" name="_wp_http_referer" value="/furgan/my-account/">
                                    <a class="furgan-Button button" onclick="signup()" style="color: white;float: none">Register</a>
                                </p>
                            </form>
                            <a href="login">Log in</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="../assets/js/jquery-1.12.4.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../action/register.js"></script>
</body>
</html>

<style type="text/css">
  #name{
    text-transform: uppercase;
  }

  #email, #username{
    text-transform: lowercase;
  }
</style>