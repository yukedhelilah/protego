<?php include "../template/header.php"; ?>

<div class="section-002" style="padding-top: 0">
    <div class="container" style="padding-top: 100px;padding-left: 20px !important;padding-right: 20px !important;text-align: center;">
        <h1 class="page-title">CONTACTS</h1>
        <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
            <ul class="trail-items breadcrumb">
                <li class="trail-item trail-begin"><a href="index"><span>HOME</span></a></li>
                <li class="trail-item trail-end active"><span>CONTACTS</span>
                </li>
            </ul>
        </div>
        <div class="row" id="content">
        </div>
    </div>
</div>

<?php include "../template/footer.php"; ?>
<script src="../action/contacts.js"></script>