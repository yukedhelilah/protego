<div class="footer-device-mobile">
    <div class="wapper">
        <div class="footer-device-mobile-item device-home">
            <a href="home" id="home-menu" class="mobile-menu">
				<span class="icon">
					<i class="fa fa-home" aria-hidden="true"></i>
				</span>
                Home
            </a>
        </div>
        <div class="footer-device-mobile-item device-home device-products">
            <a href="category" id="products-menu" class="mobile-menu">
				<span class="icon">
					<i class="fa fa-th-large" aria-hidden="true"></i>
				</span>
                Product
            </a>
        </div>
        <div class="footer-device-mobile-item device-home device-contacts">
            <a href="contacts" id="contacts-menu" class="mobile-menu">
				<span class="icon">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</span>
                <span class="text">Contact</span>
            </a>
        </div>
        <div class="footer-device-mobile-item device-home device-cart openLogin" style="display: none">
            <a href="cart" id="cart-menu" class="mobile-menu">
                <span class="icon">
                    <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                </span>
                <span class="text">Cart</span>
            </a>
        </div>
        <div class="footer-device-mobile-item device-home device-account openLogin" style="display: none">
            <a href="account" id="account-menu" class="mobile-menu">
                <span class="icon">
                    <i class="fa fa-user" aria-hidden="true"></i>
                </span>
                <span class="text">Account</span>
            </a>
        </div>
    </div>
</div>
<!-- <a href="#" class="backtotop active">
    <i class="fa fa-angle-double-up"></i>
</a> -->
<script src="../assets/js/jquery-1.12.4.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/chosen.min.js"></script>
<script src="../assets/js/countdown.min.js"></script>
<script src="../assets/js/jquery.scrollbar.min.js"></script>
<script src="../assets/js/lightbox.min.js"></script>
<script src="../assets/js/magnific-popup.min.js"></script>
<script src="../assets/js/slick.js"></script>
<script src="../assets/js/jquery.zoom.min.js"></script>
<script src="../assets/js/threesixty.min.js"></script>
<script src="../assets/js/jquery-ui.min.js"></script>
<script src="../assets/js/mobilemenu.js"></script>
<script src="../assets/js/functions.js"></script>
<script src="../action/wholejs.js"></script>
<script src="../function.js"></script>
</body>
</html>