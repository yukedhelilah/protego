<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png"/>
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,600,600i,700,700i&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/animate.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/chosen.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/jquery.scrollbar.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/lightbox.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/magnific-popup.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/slick.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/fonts/flaticon.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/megamenu.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/dreaming-attribute.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/wholecss.css"/>
    <title>Protego</title>
</head>
<body>
<header id="header" class="header style-02 header-dark header-transparent header-sticky">
    <div class="header-wrap-stick">
        <div class="header-position" style="box-shadow: -1px -1px 8px;">
            <div class="header-middle">
                <div class="furgan-menu-wapper"></div>
                <div class="header-middle-inner">
                    <div class="header-search-menu">
                        <div class="block-menu-bar">
                            <a class="menu-bar menu-toggle" href="#">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
                    </div>
                    <div class="header-logo-nav">
                        <div class="header-logo">
                            <a href="index"><img alt="Furgan" src="../assets/images/logo.png" class="logo"></a>
                        </div>
                        <div class="box-header-nav menu-nocenter">
                            <ul id="menu-primary-menu"
                                class="clone-main-menu furgan-clone-mobile-menu furgan-nav main-menu">
                                <li id="menu-item-230"
                                    class="menu-item menu-item-type-post_type menu-item-object-megamenu menu-item-230 parent parent-megamenu item-megamenu menu-item-has-children">
                                    <a class="furgan-menu-item-title" title="Home" href="home">Home</a>
                                </li>
                                <li id="menu-item-228"
                                    class="menu-item menu-item-type-post_type menu-item-object-megamenu menu-item-228 parent parent-megamenu item-megamenu menu-item-has-children">
                                    <a class="furgan-menu-item-title" title="Product" href="category">Product</a>
                                </li>
                                <li id="menu-item-229"
                                    class="menu-item menu-item-type-post_type menu-item-object-megamenu menu-item-229 parent parent-megamenu item-megamenu menu-item-has-children">
                                    <a class="furgan-menu-item-title" title="Contact" href="contacts">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="header-control">
                        <div class="header-control-inner">
                            <div class="meta-dreaming">
                                <div class="block-minicart block-dreaming furgan-mini-cart furgan-dropdown openLogin" style="display: none">
                                    <div class="shopcart-dropdown block-cart-link">
                                        <a class="block-link link-dropdown" href="cart">
                                            <span class="flaticon-cart"></span>
                                            <span class="count" id="cart"></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="menu-item block-user block-dreaming furgan-dropdown openLogin" style="display: none">
                                    <a class="block-link" href="account">
                                        <span class="flaticon-profile"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="menu-item furgan-MyAccount-navigation-link furgan-MyAccount-navigation-link--dashboard is-active">
                                            <a href="account">Account</a>
                                        </li>
                                        <li class="menu-item furgan-MyAccount-navigation-link furgan-MyAccount-navigation-link--customer-logout">
                                            <a href="javascript:;" onclick="logout()">Logout</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="block-minicart block-dreaming furgan-mini-cart furgan-dropdown notLogin" style="display: none">
                                    <div class="shopcart-dropdown block-cart-link">
                                        <a class="block-link link-dropdown" href="login">
                                            <span class="flaticon-login"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-mobile">
        <div class="header-mobile-left">
        </div>
        <div class="header-mobile-mid">
            <div class="header-logo">
                <a href="index"><img alt="Furgan" src="../assets/images/logo.png" class="logo"></a>
            </div>
        </div>
        <div class="header-mobile-right">
            <div class="header-control-inner">
                <div class="meta-dreaming">
                    <div class="menu-item block-user block-dreaming openLogin" style="display: none">
                        <a class="block-link" onclick="logout()">
                            <i class="fa fa-sign-out"></i>
                        </a>
                    </div>
                    <div class="menu-item block-user block-dreaming notLogin" style="display: none">
                        <a class="block-link" href="login">
                            <i class="fa fa-sign-in"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>